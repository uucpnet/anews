#include "param.h"
#include <sys/timeb.h>

#define	TSLEEP	8	/* number of seconds per call to sleep */
#define RETRIES 30	/* number of times to retry failed request. */
#define	NEGCHAR	'!'	/* negation character for newsgroups */

static char	BINSH[] = "/bin/sh";	/* standard shell */
static bool	dying;	/* avoid inf recursion in bomb. */
static int	popid;	/* child pid used by upopen() */
static int	pipes[2]; /* pipe file descriptors for upopen */

/*
 * Print out an error message and exit.
 * If lf, then log the error.
 * Of course, the log routines may call bomb, so
 * have to trap recursive calls.
 */

#include <errno.h>

bomb(lf, ptr)
register char *ptr;
register bool lf;
{
	register int saverr;

	saverr = errno;
	fflush(stdout);
	if (lf && !dying) {
		dying = TRUE;
		log(ptr);
		dying = FALSE;
	} else
		fprintf(stderr, "%s: %s.\n", cmdname, ptr);
	if (errno = saverr)
		perror("	perror");
	xxit(1);
}

/*
 * Bomb out, with logging
 */
xerror(s)
char *s;
{
	bomb(FALSE, s);
}

/*
 * Bomb out, with logging.
 */
lxerror(s)
char *s;
{
	bomb(TRUE, s);
}

/*
 * Give the user an warning message
 */
warn(s)
char *s;
{
	fprintf(stderr, "(Warning: %s.)\n", s);
}

/*
 * Newsystem locking.
 */
static	int	lockcount;	/* number of times we've called lock. */

#ifdef	SIGTSTP
static	int	(*ontstp)(), (*onttin)(), (*onttou)();
#endif

lock()
{
	register int i;

	if (lockcount++ == 0) {
#ifdef	SIGTSTP
		/* rti!trt -- not sure this is done right. */
		ontstp = signal(SIGTSTP, SIG_IGN);
		onttin = signal(SIGTTIN, SIG_IGN);
		onttou = signal(SIGTTOU, SIG_IGN);
#endif
retry:
		i = 0;
		while (link(SYSFILE, LOCKFILE)) {
			if (i > 0) {
				if (errno != EEXIST)
					lxerror("Cannot lock, is news SUID?");
			}
			/* The following test should really be age of LOCK */
			if (++i > RETRIES) {
				log("unlinking dead lock");
				if (unlink(LOCKFILE))
					lxerror("Cannot unlink lock");
				sleep((unsigned)TSLEEP);
				goto retry;
			}
			sleep((unsigned)TSLEEP);
		}
		errno = 0;
	}
}

unlock()
{
	if (--lockcount == 0) {
		unlink(LOCKFILE);
#ifdef	SIGTSTP
		signal(SIGTSTP, ontstp);
		signal(SIGTTIN, onttin);
		signal(SIGTTOU, onttou);
#endif
	}
	if (lockcount < 0)
		log("negative lock count");
}

/*
 * Trap interrupts.
 */
onsig(n)
int n;
{
	/*
	 * Most UNIX systems reset caught signals to SIG_DFL.
	 * This bad design requires that the trap be set again here.
	 * Unfortunately, if the signal recurs before the trap is set,
	 * the program will die, possibly leaving the lock in place.
	 */
	signal(n, onsig);
	sigtrap = TRUE;
}

/*
 * Strip trailing newlines, blanks, and tabs from 's'.
 * Return TRUE if newline was found, else FALSE.
 */
nstrip(s)
register char *s;
{
	register char *p;
	register int rc;

	rc = FALSE;
	p = s;
	while (*p)
		if (*p++ == '\n')
			rc = TRUE;
	while (--p >= s && (*p == '\n' || *p == ' ' || *p == '\t'));
	*++p = '\0';
	return(rc);
}

/*
 * Delete trailing NGDELIM.
 */
ngdel(s)
register char *s;
{
	if (*s++) {
		while (*s++);
		s -= 2;
		if (*s == NGDELIM)
			*s = '\0';
	}
}

/*
 * Append NGDELIM to string.
 */
ngcat(s)
register char *s;
{
	if (*s) {
		while (*s++);
		s -= 2;
		if (*s++ == NGDELIM)
			return;
	}
	*s++ = NGDELIM;
	*s = '\0';
}

/*
 * Dynamic string version.
 * Append NGDELIM to string.
 */
dngcat(a)
register char **a;
{
	register char *s;

	s = *a;
	s = (char *)xrealloc(s, (MTYPE)(strlen(s)+2));
	ngcat(s);
	*a = s;
}

/*
 * Check if header.h_ngs contains only valid newsgroup names;
 * exit with error if not valid.
 *
 * issub == TRUE means header.nbuf is subscription, else newsgroup list.
 *  If not rnews, prompts for each unknown newsgroup, else assumes ok.
 */


#define	NGFMAX	5000

ngfcheck(issub)
bool issub;
{
	char *ngcheck;		/* Hold NGFILE newsgroups */
	char tbuf[BUFLEN];	/* hold single header.h_ngs newsgroup */
	register char *s1, *s2, *savep;
	register FILE *fp;
	register int c;

	s1 = ngcheck = (char *)xmalloc(NGFMAX);
	fp = xfopen(NGFILE, "r");
	while (fgets(bfr, BUFLEN, fp) != NULL) {
		for (s2 = bfr; *s2 != '\0' &&
		    *s2 != ' ' && *s2 != '\t' &&
		    *s2 != ':' && *s2 != '\n';) {
			if (s1 >= &ngcheck[NGFMAX-2]) {
				log("NGFILE too long");
				*s1++ = NGDELIM;
				goto ngout;
			}
			*s1++ = *s2++;
		}
		*s1++ = NGDELIM;
	}
ngout:
	*s1 = '\0';
	xfclose(fp);
	for (s1 = header.h_ngs; *s1 != '\0';) {
		savep = s1;
		for (s2 = tbuf; (*s2++ = *s1) != NGDELIM; s1++) {
			if (!issub
			&& (strncmp(s1, "all", 3) == 0
			|| strncmp(s1, "All", 3) == 0
			|| strncmp(s1, "ALL", 3) == 0))
				xerror("Newsgroup cannot contain 'all'");
			if (index(":/ ", *s1)) {
				sprintf(bfr, "Newsgroup cannot contain '%c'", *s1);
				xerror(bfr);
			}
		}
		s1++;
		*s2 = '\0';
		s2 = tbuf;
		if (*s2 == NEGCHAR)
			s2++;
		if (*s2 == NGDELIM)
			xerror("Newsgroup must not be null");
		if ((!issub&&!ngmatch(s2,ngcheck)) || !ngmatch(ngcheck,s2)) {
			ngdel(s2);
			if (rnews)
				continue;
			/* kludge to permit yes c | news ... */
			if (!fromtty && !totty)
				continue;
			if (issub) {
				if (savep == header.h_ngs && *s1 == '\0')
					sprintf(bfr, "Is '%s' really what you want", tbuf);
				else
					sprintf(bfr, "Keep '%s' in list", tbuf);
				if ((c = answer(bfr, "ynq", 'q')) == 'q')
					goodbye();
				if (c == 'n') {
					s1 = strcpy(savep, s1);
					continue;
				}
			}
			sprintf(bfr, "Should '%s' be added to the list of 'standard' newsgroups", s2);
			if ((c = answer(bfr, "ynq", 'q')) == 'q')
				goodbye();
			if (c == 'y')
				ngfappend(s2);
			else if (!issub)
				xerror("Submit articles only to standard groups");
		}
	}
	free(ngcheck);
}

/*
 * say goodbye
 */
goodbye()
{
	xerror("Okay, goodbye");
}

/*
 * Add a newsgroup to ngfile
 */
ngfappend(s)
register char *s;
{
	register FILE *fp;

	lock();
	if (filelen(NGFILE)+strlen(s)+4 >= NGFMAX)
		lxerror("NGFILE too long, cannot append");
	fp = xfopen(NGFILE, "a");
	fprintf(fp, "%s\n", s);
	xfclose(fp);
	unlock();
	sprintf(bfr, "Newsgroup added to ngfile: '%s'", s);
	log(bfr);
}

/*
 * Return answer to the given question 'question'.
 * Answer must be one of the characters in 'validresp'.
 * An interrupt or typing 'x' is equivalent to typing 'qchar'.
 */
answer(question, validresp, qchar)
char *question, *validresp, qchar;
{
	register int c, ans;
	register FILE *fp;

	fp = NULL;
	do {
		sigtrap = FALSE;
		fflush(stdout);
		fprintf(stderr, "%s? [%s] ", question, validresp);
		if (fp == NULL) {
			fp = stdin;
			if (!fromtty)
				if ((fp = fopen("/dev/tty", "r")) == NULL)
					lxerror("Cannot obtain reply");
		}
		ans = c = getc(fp);
		while (c != '\n') {
			if (sigtrap || c == EOF) {
				clearerr(fp);
				ans = qchar;
				fprintf(stderr, "\n");
				break;
			}
			c = getc(fp);
		}
		if (ans == 'x')
			ans = qchar;
		errno = 0;
	} while  (index(validresp, ans) == NULL);
	return(ans);
}

/*
 * Newsgroup matching.
 *
 * nglist is a list of newsgroups.
 * sublist is a list of subscriptions.
 * sublist may have "meta newsgroups" in it.
 * All fields are NGDELIM separated,
 * and there is an NGDELIM at the end of each argument.
 *
 * Currently implemented glitches:
 * sublist uses 'all' like shell uses '*', and '.' like shell '/'.
 * If subscription X matches Y, it also matches Y.anything.
 */
ngmatch(nglist, sublist)
register char *sublist;
char *nglist;
{
	register char *n, *s;
	register int rc;

	rc = FALSE;
	for (n = nglist; *n != '\0' && rc == FALSE;) {
		for (s = sublist; *s != '\0';) {
			if (*s != NEGCHAR)
				rc |= ptrncmp(s, n);
			else
				rc &= ~ptrncmp(++s, n);
			while (*s++ != NGDELIM);
		}
		while (*n++ != NGDELIM);
	}
	return(rc);
}

/*
 * Compare two newsgroups for equality.
 * The first one may be a "meta" newsgroup.
 */
ptrncmp(ng1, ng2)
register char *ng1, *ng2;
{
	while (*ng1 != NGDELIM) {
/* hardwired 'all' ('ALL') */
		if ((*ng1 == 'a' || *ng1 == 'A')
		 && ((ng1[1] == 'l' && ng1[2] == 'l')
		  || (ng1[1] == 'L' && ng1[2] == 'L'))) {
			ng1 += 3;
			while (*ng2 != NGDELIM && *ng2 != '.')
				if (ptrncmp(ng1, ng2++))
					return(TRUE);
			return (ptrncmp(ng1, ng2));
		} else if (*ng1++ != *ng2++)
			return(FALSE);
	}
	return (*ng2 == '.' || *ng2 == NGDELIM);
}

/*
 * Remove newsgroups in 'a' not subscribed to by 'b'.
 * Return TRUE if squashed any out, else FALSE
 */
ngsquash(ap, bp)
register char *ap, *bp;
{
	register char *tp;
	register bool flag;

	/* replace NGDELIM by '\0' in a */
	for (tp = ap; *tp != '\0'; tp++)
		if (*tp == NGDELIM)
			*tp = '\0';
	/* ap = building, tp = checking. */
	flag = FALSE;
	tp = ap;
	while (*tp != '\0') {
		ngcat(strcpy(lbfr, tp));
		if (ngmatch(lbfr, bp)) {
			while ((*ap++ = *tp++) != '\0')
				;
			ap[-1] = NGDELIM;
		} else {
			flag = TRUE;
			while (*tp++ != '\0');
		}
	}
	*ap = '\0';
	return(flag);
}

/*
 * Exec the shell with command s.
 * This version reverts permissions,
 * then if file is non-NULL, stdin is directed from it.
 * If the user has defined a SHELL environment variable,
 * then that shell is used, else /bin/sh.
 * Called with fsubr(ushell, s, file)
 */
ushell(s, file)
char *s, *file;
{
	register char *sh, *shell;

	revert();
	newstdin(file);
	if ((shell = getenv("SHELL")) == NULL)
		shell = BINSH;
	sh = sname(shell);
	fflush(stdout);
	execl(shell, sh, "-c", s, 0);
	xshell(s);
}

/*
 * Exec the shell with command s.
 * If file is non-NULL, stdin is directed from it,
 * then it reverts permissions,
 * Called with fsubr(nshell, s, file)
 */
nshell(s, file)
char *s, *file;
{
	newstdin(file);
	/* just revert [ug]id, not environ or PATH */
	setgid(gid);
	setuid(uid);
	xshell(s);
}

/*
 * Exec the shell.
 * Called with fsubr(xshell, s, NULL)
 */
xshell(s)
char *s;
{
	fflush(stdout);
	execl(BINSH, "sh", "-c", s, 0);
	lxerror("No shell!");
}

/*
 * If s is non-null, attempt to redirect input from it.
 */
newstdin(s)
register char *s;
{
	if (s && *s) {
		close(0);
		if (open(s, 0))
			lxerror(s);
	}
}

/*
 * User-level (secure) version of popen.
 * Runs command line 's'.
 * If (flag) then 's' is directly executed,
 * otherwise a shell interprets 's'.
 */
FILE *
upopen(s, flag)
char *s;
bool flag;
{
	int xpopen();

	if (pipe(pipes) < 0)
		return(NULL);
	popid = fsubr(xpopen, s, (char *)flag);
	close(pipes[0]);
	return(fdopen(pipes[1], "w"));
}

/*
 * Local pclose routine.
 * If (flag) then signals are noted by sigint,
 * otherwise signals are disabled while waiting.
 */
npclose(fp, flag)
FILE *fp;
bool flag;
{
	fclose(fp);
	if (flag)
		return(xfwait(popid));	/* signals on */
	return(fwait(popid));		/* signals off */
}

/*
 * Child process for upopen()
 */
xpopen(s, t)
register char *s;
char *t;
{
	close(pipes[1]);
	if (dup2(pipes[0], 0) < 0)
		lxerror(s);
	if ((bool)t) {
		revert();
		execlp(s, sname(s), (char *)NULL);
	}
	ushell(s, (char *)NULL);
}

/*
 * Fork and call a subroutine with two args.
 * Calls xerror if child's exit status is not zero.
xfwsubr(f, s1, s2)
int (*f)();
char *s1, *s2;
{
	register int status;

	if ((status = fwsubr(f, s1, s2)) != 0)
		lxerror("Child process failed");
}
 */

/*
 * Fork and call a subroutine with two args.
 * Returns child's exit status.
 */
fwsubr(f, s1, s2)
int (*f)();
char *s1, *s2;
{
	return(fwait(fsubr(f, s1, s2)));
}

/*
 * Fork and call a subroutine with two args.
 * Return pid without waiting.
 */
fsubr(f, s1, s2)
int (*f)();
char *s1, *s2;
{
	register int pid;

	pid = dofork();
	if (pid == 0)
		exit((*f)(s1, s2));
	return(pid);
}

/*
 * Wait on a child process, ignoring signals.
 */
fwait(pid)
int pid;
{
	register (*oldhup)(), (*oldintr)(), (*oldquit)();
	register int status;

	oldhup = signal(SIGHUP, SIG_IGN);
	oldintr = signal(SIGINT, SIG_IGN);
	oldquit = signal(SIGQUIT, SIG_IGN);
	status = xfwait(pid);
	signal(SIGHUP, oldhup);
	signal(SIGINT, oldintr);
	signal(SIGQUIT, oldquit);
	return(status);
}

/*
 * Wait for child to finish. Signals not touched
 */
xfwait(pid)
int pid;
{
	register int w;
	static int status;

	while ((w = wait(&status)) != pid && (w != -1 || errno != ECHILD))
		;
	if (w == -1)
		status = -1;
	return(status);
}

/*
 * fork the current process, trying hard to create the child.
 */
dofork()
{
	register int pid, i;

	fflush(stdout);
	i = 0;
	while ((pid = fork()) == -1) {
		if (++i > RETRIES)
			lxerror("Cannot fork");
		sleep((unsigned)TSLEEP);
	}
	return(pid);
}

/*
 * revert permissions to those of the RUID
 */
revert()
{
	setgid(gid);
	setuid(uid);
	umask(savmask);
	environ = savenv;
}

/*
 * Exit and cleanup.
 */
xxit(status)
int status;
{
	while (lockcount > 0)
		unlock();
	exit(status);
}

/*
 * Get user name and home directory.
 */
getuser()
{
	register struct passwd *p;

	if (username)
		return;
	if ((p = getpwuid(uid)) == NULL)
		lxerror("Cannot get user's name");
	dstrcpy(&username, p->pw_name);
	dstrcpy(&userhome, p->pw_dir);
	dstrcpy(&header.h_from, username);
}

/*
 * Put a unique name into header.name.
 */
getname()
{
	long seqn;
	register FILE *fp;

	fp = xfopen(SEQFILE, "r");
	fgets(bfr, BUFLEN, fp);
	xfclose(fp);
	seqn = atol(bfr) + 1;
	fp = xfopen(SEQFILE, "w");
	fprintf(fp, "%ld\n", (long)seqn);
	xfclose(fp);
	sprintf(header.h_name, "%s.%ld", SYSNAME, (long)seqn);
}

/*
 * Move cancelled or otherwise banished news into the CAND directory.
 * If flag is true, leave behind a zero-length "stub".
 */
canned(oname, fname, flag)
register char *oname, *fname;
bool flag;
{
	register FILE *fp;
	struct hbuf h;

	/* Add extended nindex entry to CNINDEX */
	h_init(&h);
	if ((fp = h_read(&h, oname)) != NULL) {
		xfclose(fp);
		sprintf(bfr, "%s/c_nindex", NEWSD);
/*		lock();  rti!trt: too slow */
		if ((fp = fopen(bfr, "a")) != NULL) {
			ngdel(strcpy(bfr, h.h_ngs));
			fprintf(fp, "%s:%ld:%s:%s\n", h.h_name,
				(long)atot(h.h_rdate), bfr, h.h_title);
			xfclose(fp);
		}
/*		unlock();  rti!trt: too slow */
	}
	h_free(&h);

	/* Link file over to 'canned' directory */
	sprintf(bfr, "%s/%s", CAND, fname);
	link(oname, bfr);	/* no diagnostic if link fails */
	unlink(oname);
	if (flag)
		xfclose(xfopen(oname, "w"));
}

/*
 * Generate at p the filename NEWSD/file
 */
char *
mknname(p, file)
register char *p, *file;
{
	sprintf(p, "%s/%s", NEWSD, file);
	return(p);
}

/*
 * Generate at p the filename ARTD/sub#/file,
 * where # is the last one or two digits in the filename.
 */
char *
mkaname(p, file)
register char *p, *file;
{
#ifdef	SUBDIR
	register char *s;
	s = file; while (*s++)
		;
#if	(SUBDIR <= 10)
	s -= 2;
	sprintf(p, "%s/%d/%s", ARTD, atoi(s), file);
#else
	s -= 3;
	sprintf(p, "%s/%02d/%s", ARTD, atoi(s), file);
#endif
#else
	mknname(p, file);
#endif
	return(p);
}

/*
 * return TRUE if string is syntactically correct article name.
 * else return FALSE.
 */
bool
isarticle(s)
register char *s;
{
	register int c, n;

	/* match [^.][^.]*. */
	n = 0;
	while ((c = *s++) != '\0' && c != '.')
		n++;
	if (n == 0)
		return(FALSE);

	/* match ^[0-9][0-9]*$ */
	while (c = *s++) {
		if (!isdigit(c))
			return(FALSE);
		n = 0;
	}
	return(n == 0);
}

/*
 * Generate at p a unique filename of form NEWSD/s#pid
 */
char *
nmktemp(p, s)
register char *p, *s;
{
	mknname(p, s);
	strcat(p,"XXXXXX");
	mktemp(p);
	errno = 0;	/* reset error indicator */
	return(p);
}

static char	MEMERR[] = "Cannot allocate memory";
/*
 * Memory allocator, with error checking
 */
MALIGN
xmalloc(size)
MTYPE size;
{
	register MALIGN mp;

	if ((mp = (MALIGN)malloc(size)) == NULL)
		lxerror(MEMERR);
	return(mp);
}

/*
 * Memory re-allocator, with error checking.
 */
MALIGN
xrealloc(p, size)
register char *p;
MTYPE size;
{
	register MALIGN mp;

	if ((mp = (MALIGN)realloc(p, size)) == NULL)
		lxerror(MEMERR);
	return(mp);
}

/*
 * Dynamic storage version of strcpy.
 * If *a is not NULL, free the area it points to.
 * Then make copy of string b in malloc-ed area,
 * setting *a to point to the area.
 */
char **
dstrcpy(a, b)
register char **a, *b;
{
	register char *s;

	s = *a;
	if (s)
		free(s);
	strcpy((s = (char *)xmalloc((MTYPE)(strlen(b)+1))), b);
	*a = s;
	return(a);
}

/*
 * Dynamic storage version of strcat.
 * Reallocs *a and appends string b to it.
 * Ensures that result fits in at most MAXLEN characters.
 */
char **
dstrcat(a, b)
register char **a, *b;
{
	register char *s;
	register MTYPE len;

	s = *a;
	if (!s)
		return(dstrcpy(a,b));
	len = strlen(s) + strlen(b) + 1;
	if (len > MAXLEN) {
		sprintf(bfr, "Too long:%.40s:%.40s", s, b);
		lxerror(bfr);
	}
	strcat((s = (char *)xrealloc(s, (MTYPE)len)), b);
	*a = s;
	return(a);
}

/*
 * Deallocate dynamically allocated string.
 * Free area *a points to, and make *a NULL.
 */
dfree(a)
register char **a;
{
	if (*a) {
		free(*a);
		*a = NULL;
	}
}

/*
 * Return filelen(NEWSD/file)
 */
off_t
afilelen(file)
char *file;
{
	mkaname(bfr, file);
	return(filelen(bfr));
}

/*
 * Return length of file in bytes.
 * Return -1 if cannot stat file.
 */

#include <sys/stat.h>

off_t
filelen(file)
char *file;
{
	struct stat st;

	if (stat(file, &st))
		return(-1);
	return(st.st_size);
}

/*
 * Local open routine.
 */

#include <sgtty.h>

FILE *
xfopen(name, mode)
register char *name, *mode;
{
	register FILE *fp;

	if ((fp = fopen(name, mode)) != NULL)
		goto okopen;
	lock();
	fp = fopen(name, mode);
	unlock();
	if (fp != NULL)
		goto okopen;
	sprintf(bfr, "Cannot open %s (%s)", name, mode);
	lxerror(bfr);

okopen:
	ioctl(fileno(fp), FIOCLEX, NULL);	/* security measure */

#ifdef	SUIDBUG
	/* kludge fixup when root invokes news */
	if (duid != 0 && (*mode == 'w' || *mode == 'a'))
		chown(name, duid, dgid);
#endif

	return(fp);
}

/*
 * Close file pointer fp, checking for errors.
 */
xfclose(fp)
register FILE *fp;
{
	if (fp == NULL)
		lxerror("xfclose: NULL file pointer");
	fflush(fp);
	if (ferror(fp))
		lxerror("xfclose: file error");
	if (fclose(fp))
		lxerror("xfclose: close error");
}

/*
 * Convert a time field from ascii to time_t.
 * Returns -1 on error.
 */
/*ARGSUSED*/
time_t
atot(s)
char *s;
{
#ifdef	GETDATE
	return(getdate(s, (struct timeb *)0));
#else
	return(-1);
#endif
}

/*
 * Return pointer to next space or tab character,
 * or to the NULL at the end of the string.
 */
char *
space(s)
register char *s;
{
	register int c;

	while (c = *s++)
		if (c == ' ' || c == '\t')
			break;
	return(s-1);
}

/*
 * Return pointer to next non-space-or-tab character,
 * or to the NULL at the end of the string.
 */
char *
nonspace(s)
register char *s;
{
	register int c;

	while (c = *s++)
		if (c != ' ' && c != '\t')
			break;
	return(s-1);
}

/*
 * Return TRUE if 's' begins with the local system name,
 * else FALSE
 */
islocal(s)
register char *s;
{
	register int len, c;

	if (strncmp(s, SYSNAME, len = strlen(SYSNAME)))
		return(FALSE);
	c = s[len];
	if (c == '\0' || index(NETCHRS, c) || index(" .", c))
		return(TRUE);
	return(FALSE);
}

/*
 * Convert article ID/message ID to article ID.
 * This is a real kludge, because article IDs are obsolete,
 * but it is easier than upgrading A news to the new 'standard'
 */
fixartname(a, s)
register char *a, *s;
{
	long n;
	register char *p;

	/* If Message ID, change to article ID */
	if (*s == '<') {
		n = atol(++s);
		while (*s && *s != '@')
			s++;
		p = ++s;
		while (*s && *s != '.' && *s != '>')
			s++;
		*s = '\0';
		strcpy(a, p);
		sprintf(a+strlen(a), ".%ld", n);
		log(a);
	}
	else
		strcpy(a, s);
}
