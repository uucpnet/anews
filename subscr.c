#include "param.h"

/*
 * Change or display a user's subscription list.
 */

static char	SNGERR[] = "Subscription list too long";

subscr()
{
	struct urec urec;
	register bool newuser;

	if (*header.h_ngs) {
		/* All users must subscribe to SNGMUST */
		dngcat(dstrcat(&header.h_ngs, SNGMUST));

		newuser = TRUE;
		u_openr();
		while (u_read(&urec)) {
			if (uid == urec.u_uid) {
				newuser = FALSE;
				strcpy(bfr, header.h_ngs);
				dstrcpy(&header.h_ngs, urec.u_sngs);
				dstrcat(&header.h_ngs, bfr);
				break;
			}
		}
		u_close();
		if (newuser) {
			urec.u_uid = uid;
			time(&(urec.u_date));
		}
		sngscanon(header.h_ngs);
		ngfcheck(TRUE);
		sngscanon(header.h_ngs);
		strcpy(urec.u_sngs, header.h_ngs);
		if (strlen(urec.u_sngs) >= MAXLEN-40)
			xerror(SNGERR);
		lock();
		u_openm();
		u_write(&urec);
		while (u_read(&urec))
			if (urec.u_uid != uid)
				u_write(&urec);
		u_close();
		newsub(&urec);
		unlock();
	}
	else {
#ifdef	PW_ADD
		dstrcpy(&header.h_ngs, SNGDFLT);
#else
		header.h_ngs[0] = '\0';
#endif
		u_openr();
		while (u_read(&urec)) {
			if (uid == urec.u_uid) {
				dstrcpy(&header.h_ngs, urec.u_sngs);
				break;
			}
		}
		u_close();
	}
	ngdel(header.h_ngs);
	printf("Subscription list = %s\n", header.h_ngs);
}

/*
 * Canonicalize subscription newsgroup list.
 * Each newsgroup is considered left-to-right,
 * and is dropped if it is disabled by a following '!',
 * made unnecessary due to an earlier or later inclusive newsgroup,
 * or if it is itself a '!' newsgroup but has no effect.
 */
static
sngscanon(p)
register char *p;
{
	register char *tp, *q, *r;

	/* make bfr a null-separated sequence of newsgroups. */
	q = p;
	tp = &bfr[0];
	while (*tp = *q++) {
		if (tp >= &bfr[MAXLEN-3])
			xerror(SNGERR);
		if (*tp++ == NGDELIM)
			*tp++ = '\0';
	}
	/* p = building, tp checking */
	*p = '\0';
	tp = &bfr[0];
	while (*tp) {
		q = tp;
		while (*tp++)
			;
		/* p->head, q->candidate, tp->tail */
		/* drop q if it does not turn off something in the head */
		if (*q == '!' && !ngmatch(q+1,p) && !ngmatch(p,q+1))
			continue;
		/* drop q if it is subscribed to by the head */
		if (*q != '!' && ngmatch(q,p))
			continue;
		/* drop q if  it is subscribed to or turned off in the tail */
		for (r = tp; *r;) {
			if (ngmatch(q+(*q == '!'),r+(*r == '!')))
				goto contin;
			while (*r++)
				;
		}
		ngcat(strcat(p, q));
	contin:;
	}
}

/*
 * Produce annotated subscription list
 */
sublist()
{
	register FILE *ifp, *ofp;
	register int c;
	register bool are_page;

	ifp = xfopen(SUBLIST, "r");
	ofp = stdout;
	are_page = FALSE;
	if (pager && *pager && totty) {
		ofp = upopen(pager, TRUE);
		are_page = TRUE;
	}
	while ((c = getc(ifp)) != EOF && !sigtrap)
		putc(c, ofp);
	xfclose(ifp);
	if (are_page && npclose(ofp, TRUE) && !sigtrap) {
		pager = NULL;
		fprintf(stderr, "Pagination disabled\n");
		sublist();
	}
}
