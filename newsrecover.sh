#! /bin/sh
# MCNC	newsrecover.sh	1.2	8/30/82	18:08:53
: ${NEWSDIR=/usr/spool/news}
: ${NEWSREC=/usr/lib/newsrec}
: ${NEWNINDEX=/usr/spool/news/newnindex}

cd $NEWSDIR
for i in *
do
case $i in
	[0-9] | [0-9][0-9])
		cd $NEWSDIR; cd $i; $NEWSREC ;;
	*) ;;
esac
done |

sed -e '/Newsgroups: /s///' |

sort -t: -n -r +1 > $NEWNINDEX
