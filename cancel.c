#include "param.h"

/*
 * Cancel a news item.
 */
bool
cancel(file)
register char *file;
{
	struct hbuf h;
	register FILE *fp;
	register int c;
	register bool rc, mkstub;

	rc = FALSE;
	h_init(&h);
	if ((fp = ah_read(&h, file)) == NULL)
		goto ret;
	xfclose(fp);
	if (!pathmatch(&h)) {
		printf("Not contributor.  ");
		goto ret;
	}
	if (sigtrap)
		goto ret;

	/* kludge mainly to permit "yes c | news -n ALL -b -2 weeks" */
	mkstub = fromtty && totty && atot(h.h_adate) > atot(uucancel);

#ifdef CTL_MSG
	if (mkstub && broadcast(file, (char *)1)) {
		fprintf(stderr, "This article was (probably) broadcast.\n");
		if ((c = answer("Cancel (e)verywhere, or just (l)ocally", "elq", 'q')) == 'q')
			goto ret;
		if (c == 'e') {
			printf("Okay, please be patient.\n");
			sprintf(bfr, "%s -i 'cancel %s' -c 'cancel %s' -n %s",
				cmdname, file, file, h.h_ngs);
			rc = (fwsubr(nshell, bfr, "/dev/null") == 0);
			goto ret;
		}
	}
	if (sigtrap)
		goto ret;
#endif
	mkaname(lbfr, file);
	canned(lbfr, file, mkstub && !islocal(file));
	rc = TRUE;
ret:
	h_free(&h);
	return(rc);
}

/*
 * Path name match check.
 * The ngmatch routine is probably misused here.
 */
static bool
pathmatch(hp)
register struct hbuf *hp;
{
	register bool rc;

	getuser();
	sprintf(bfr, "%d,", uid);
	if (ngmatch(CNID, bfr))
		return(TRUE);
	sprintf(bfr, "%s,", username);
	if (ngmatch(CNID, bfr))
		return(TRUE);
	rc = (strcmp(hp->h_from, username) == 0);
	return(rc);
}
