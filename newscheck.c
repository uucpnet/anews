/*
 * Exit with value 0 if user has news, else 1.
 * If 'y' option is given, prints message if user has news.
 * If 'n' option is given, prints message if user does not have news.
 *
 * This program is intended to be small enough to fit into
 * a single disk block but on many systems the stdio package
 * gets dragged in anyway, quadrupling its size.  Oh well.
 */
char	BITFILE[] = NEWSDIR/bitfile";

extern	long	lseek();

main(argc, argv)
int argc;
char **argv;
{
	register int y, n, rc;

	y = 0;
	n = 0;
	while (--argc > 0) {
		for (argv++; **argv; ++*argv) {
			if (**argv == 'y')
				y++;
			if (**argv == 'n')
				n++;
		}
	}
	rc = newscheck(getuid());
	if (rc) {
		if (y)
			write(1, "You have news.\n", 15);
	}
	else {
		if (n)
			write(1, "No news.\n", 9);
	}
	exit(!rc);
}

/* return 1 if user has news, else 0. */
newscheck(uid)
register int uid;
{
	register int fd;
	static char c;
	fd = open(BITFILE, 0);
	lseek(fd, (long)(uid>>3), 0);
	read(fd, &c, 1);
	close(fd);
	return((c >> (uid&07)) & 01);
}
