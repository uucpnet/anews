New option: "-r" prints articles newest first (default: oldest first).
Mostly used as "news -ar" to review recent news.

-t argument no longer implies -an all, and selection is restricted
to titles containing one or more of the specified strings.

Speedups:
.uindex and .nindex files: ":general" dropped.
Sorted newest date first, so searches usually take 1 disk read.
News is no longer locked when just reading news, which is faster,
but if an article is inserted just as a user finishes reading new news
the BITFILE may erroneously record no new news for the user.
That is corrected when the next article is inserted (or cancelled).
News itself is not affected since it does not use BITFILE.

.sys: each line may have two additional fields.
First field is still sysname, second field is still subscription list.
Optional fourth field is command line used for article transmission;
the article is presented as standard input to the command line.
If fourth field is missing "uux - sysname!rnews" is assumed.

ngmatch: supports "not" newsgroups.  e.g.: "news -n net.all !net.news"
prints all net articles except those submitted only to net.news.
"news -s all !net.all" subscribes only to "local" articles.

"q" reply updates last read time. "x" does not.

system names are only significant to 8 chars.

(Of interest to Berkeley only (hopefully)):
newscheck.c fast newscheck program.
The '-c' option is now supported, but not documented.
'w' and 'r' replies now support 'w-' and 'r-'. sigh.

FREEZE DATE FOR RELEASE 1.2
Thu Jun  5 10:55:52 EDT 1980


options processor kludged up to allow "news -n test -i junk"
 in addition to "news -i junk -n test".

lock() checks errno if link fails, and exits if not EEXIST.
	DEADTIME increased to 150 seconds.

Checks for write errors on temporary .uindex and .nindex,
	so disk run-outs are less fatal.

Checks for close() errors, to catch a few more problems.

Change uneti() to print more diagnostics about ignored articles.

change uurec so can handle "To" lines inbetween the from lines
	and the 'N' lines.

change cancelling so that .nindex is not updated every cancel,
	but only at the end (via ninclean()).  MUCH faster.

changed main so that a full path name may be specified for 'rnews'
	and it will still find the 'r'.

sped up bitfile update for rnews, news -i, and news -s. MUCH faster.

changed input so a truly unique temp file name is used.
	this eliminates an obscure overwrite bug.

uux is now run with "-r" option (do not poll remote sites). MUCH faster.

Article header displayed differently -- includes unique article name.

News security has been enhanced.  Handles PATH, alarm FIOCLEX, etc.

Improved diagnostics, especially for inbound news.

makebits() and reader() share "nmalloc()" core allocator,
	which uses a static array.

Leading+trailing blanks on article title removed,
	as are trailing blank lines on the article.

FREEZE DATE FOR RELEASE 1.5
Mon Aug 24 01:01:43 EDT 1981

GENERAL replaced by INGDFLT, SNGDFLT, SNGMUST.

If xfopen()'s fopen() fails, it locks and retries.

news -s is now incremental-- "news -s foo" adds newsgroup foo,
	"news -s !foo" removes it, "news -s !all dept"
	changes newsgroup list to just dept (and SNGMUST).

signals are now ignored by rnews.

EOF in readr() is equivalent to typing "q".
If "q" is typed and the current article was interrupted,
its "READ" flag is turned off.
This permits one to reread the current article if it was interrupted.
it also handles phone line drops in a reasonable way.

SUBLEN (subscription length) and PATHLEN (system path sequence)
have been split from BUFLEN, permitting enormous SUBLEN (e.g. 2kbytes).

Dynamic storage (malloc(III), free(III)) used for all
allocations of more than 100 bytes.
"Dynamic string" routines dstrcpy(), dstrcat(), dngcat()
support large strings.
In readr(), a small artlist is tried (50 articles),
switching to the full ARTSIZ element list if necessary.

Old "-b" option replace by "before" option:
"news -a last week -b yesterday" prints news entered after a week ago
but before yesterday.  This feature simplfies news cancelling:
	yes c | news -n all -b 2 weeks ago;     : cancel old articles.
	news -E;	: dropped expired articles, stubs.  see below.

options parser kludged to accept "-"s in dates, eg. "news -a -2 weeks".

Escaping from news to change one's subscription list now works.

Rewriting .uindex last_read_time is now done in place when possible.
If stdio supports read/write IO, it goes even faster.

CNID is a list of people permitted to cancel any article.

Obscure bug due to read errors on /etc/passwd has been fixed.
/etc/passwd-unidex synchronize is bypassed if /etc/passwd is bad.

New "local.h" file simplifies local mods.  Read it for details.
New options include a log file and specifiable mailer.
The log file records events of interest to an administrator,
including all control message requests.
Two flavors of mailer are supported: vanilla and ones
which take a "-s 'subject here'" argument (ala UCB Mail).
In the latter case, the "r" reply provides an automatic subject.

Handle unknown newsgroups better (in ngfcheck)
	1) if in rnews - allow with no change to .ngfile
	2) if in news -n, -s ask user if wanted - no change to .ngfile
	3) if in news -i ask user if create - change to .ngfile if yes.

If a submitted article is short or has nonascii characters,
the submitter is asked to confirm the submission request.
The submitter is warned if s/he does not subscribe to the submitted article.

"news duke.1620 duke.1621" reads those articles.
  the -c, -l, -p, and -r flags are also legal after an article list.

u_update modified to never write a smaller time over a bigger one.

subdirectory (hashed -- used last digit of sequence number)
	efficiency enhancement.  MUCH faster.

Checks for interrupts while scanning past articles which
 are in .nindex but don't exist.

"X all" broadcasts the articles to all systems which
subscribe to the article.

Answer() subroutine used to ask questions: tries stdin,
or /dev/tty if stdin not tty.

Support for multiple art formats added.  Currently supported
are the old A format, and the current B format.
The B format when used internally may have lines that are
not transmitted or printed.  Currently the article length
(in lines) is saved this way.  Eventually archiving status
will also be saved thusly.

The third field of the .sys file is now parsed up to a comma
as a flag line.  Each character is supposed to be a transmission
option.  Currently supported are A and B, which tell the
format to use on transmission.  Default is 'A'.  (Note:
Berkeley's default is 'B' ! ).

Control messages now supported.
A control message can be generated via, for example:
	news -i sendsys -n net.msg.ctl  -c sendsys
Also, when a user cancels a recent network article,
he is asked if the cancel should be local or net-wide.

Control messages are "canned", not stored as articles;
however, a zero-length "stub" remains in the article directory
so duplicate control messages will be caught.
Stubs are also left behind by cancelled network articles.

Expiration dates now supported:
	news -i XOM meeting  -n dept  -e 5pm tomorrow < artfile
the article will expire at 5pm that day.

Expired articles and stubs are cleaned up by "expunging" them:
	news -E [optional expunge date here]
This cancels all expired articles,
deletes any stubs older than the expunge date (default: 10 days ago),
and any articles not in nindex and not reference since the expunge date.
This should be run daily along with bulk cancelling:
	yes c | news -an all -b -2 weeks

Both 'all' and 'ALL' are accepted as the newsgroup glob character. yuck.

If "PAGER" is defined in the environment, then all articles
of more than 20 lines are run through said program.

The write reply can pipe its output to a process, e.g. "? w |lpr"
	(Ex doesnt work this way.  Should this feature be killed?)

Routine "local.c" handles site-dependent routines,
especially sitename() which returns local system name.

new "-f" ("referencing", "follow up") option used by "f" reply.
	news -i <title> [-n <newsgroups>] -f <referenced articles>
Generates "References:" header.

The '.'s on the beginning of files have been removed to cater to 4.xbsd.
So to run the new news one must (a) rename the files in /usr/spool/news
(e.g. 'mv .uindex uindex'), (b) FIX DAILY SHELL SCRIPTS, (c) FIX UTILITY
PROGRAMS (uurec, whatever).
Sorry this is such a pain, but the conversion should be done sometime.

nindex is ordered oldest first, so new articles are just appended.
A new "n_openl(date)" locates the first article which arrived after
the given date, so recent articles can still be located quickly.
This change considerable reduces the IO done by rnews
and the time which news spends locked up.

If only a few articles are cancelled in a session, the nindex and bitfile
cleanups are done in the background so the user need not wait.

Infinite 'cancel loops' caused by running 'yes c | news -b -2 weeks'
with insufficient permission are now caught.

ngfile can now be as large as 5000 bytes.

"news -S" prints the file /usr/spool/news/sublist which should
be a summary of newsgroups such as the one Adam Buchsbaum produces.

A default PAGER program is available.  See local.h.

Canceled articles are logged in the file /usr/spool/news/c_nindex.

When inserting an article, a few "~" escapes are available:
	~p, ~e, ~v, ~!, ~?, similar to the Berkeley mail escapes.

Default transmission format is now "B" format.

nindex file is now in ascending date order, so article insertion
is done by appending to the file.

ninclean routine much faster now when cancelling many articles

makebits routine much faster now on large nindex files.

Followups to net.general go to net.followup.
