#include "param.h"
#define BTSZ ((USIZE+7)/8)

/*
 * Reconstruct the bit file.
 */
makebits()
{
	struct nrec nrec, *nbase, *nptr, *tnptr;
	struct urec *urecp;
	char bitbuf[BTSZ];	/* incore bit file */
	register int i, nnrec, nfree;
	bool eofflag, found;
	register char *s;
	FILE *bits;

	fflush(stdout);
	/* To avoid fragmentation, must use no more space than did artlist */
	nnrec = ARTSIZ*sizeof(struct artlist)/sizeof(*nbase);
	nbase = (struct nrec *)xmalloc((MTYPE)nnrec*sizeof(struct nrec));
	urecp = (struct urec *)xmalloc(sizeof(struct urec));
	/* set all bits to "up to date" (0) */
	for (i = 0; i < BTSZ; i++)
		bitbuf[i] = 0;

	lock();
	u_openr();
	n_openr();	/* open nindex */
	for (eofflag = FALSE; !eofflag;) {
		/* loop once per section of nindex file. */
		eofflag = TRUE;

		/* Prepare to hash (!) nindex newsgroups */
		for (i = 0; i < nnrec; i++)
			nbase[i].n_date = -1;
		nfree = nnrec;
		while (n_read(&nrec)) {
			/* bust insertion newsgroups into separate newsgroups */
			while (nrec.n_ings[0] != '\0') {
				strcpy(bfr, nrec.n_ings);
				if ((s = index(bfr, NGDELIM)) == NULL)
					lxerror("no delim!");
				s++;
				strcpy(nrec.n_ings, s);
				*s = '\0';
				/* compute hash function */
				i = 0;
				s = bfr;
				while (*s)
					i = (i + *s++) * 2;
				i = (unsigned)i % nnrec;
				/* Search for newsgroup */
				found = FALSE;
				s = bfr;
				for (;;) {
					if (nbase[i].n_date < 0)
						break;
					if (strcmp(nbase[i].n_ings, s) == 0) {
						found = TRUE;
						break;
					}
					if (++i >= nnrec)
						i = 0;
				}
				if (found) {
					/* Just update newsgroup date */
					if (nrec.n_date > nbase[i].n_date)
						nbase[i].n_date = nrec.n_date;
				}
				else {
					/* Add newsgroup to table */
					nbase[i].n_date = nrec.n_date;
					strcpy(nbase[i].n_ings, s);
					--nfree;
					if (nfree <= 2)
						break;	/* better than dying */
				}
			}
			/* do not let table get too full */
			if (nfree < 5 + nnrec/10) {
				eofflag = FALSE;
				break;
			}
		}

		/* Compress table for linear search */
		for (tnptr = nptr = nbase; nptr < &nbase[nnrec]; nptr++)
			if (nptr->n_date >= 0) {
				tnptr->n_date = nptr->n_date;
				strcpy(tnptr->n_ings, nptr->n_ings);
				tnptr++;
			}
		tnptr->n_date = -1;

		/*
		 * Got this piece of nindex in, now read uindex
		 * and set all of the appropriate bits.
		 * Note: hashing a uindex entry to each of its newsgroups
		 * might save some more cpu time.
		 * Current speedup is to squeeze out null entries (above).
		 */
		u_rewind();
		while (u_read(urecp)) {
			for (nptr = nbase; nptr->n_date >= 0; nptr++) {
				/* check dates. */
				if (nptr->n_date <= urecp->u_date)
					continue;

				/* check for newsgroup match. */
				if (!ngmatch(nptr->n_ings, urecp->u_sngs))
					continue;

				bitbuf[urecp->u_uid>>3] |=
					(1<<(urecp->u_uid&07));
				break;
			}
		}
	}
	n_close();
	u_close();

	/* write out the bits. */
	bits = xfopen(BITFILE, "w");
	fwrite(bitbuf, 1, BTSZ, bits);
	xfclose(bits);
	unlock();
	free((char *)urecp);
	free((char *)nbase);
	bitup = FALSE;
}

/*
 * To improve efficiency,
 * and since stdio does not (officially)
 * support both read and write i/o on the same stream,
 * the UNIX open/lseek/read/write/close system calls
 * are used here.
 */

static char	BFERR[] = "Cannot open BITFILE for update";

/*
 * change bitfile to indicate that there is NO news for id.
 */
nonews(id, check)
id_t id;
bool check;
{
	register int bitfd, bit;
	static char c;

	lock();
	if ((bitfd = open(BITFILE, 2)) < 0)
		lxerror(BFERR);
	lseek(bitfd, (long)(id>>3), 0);
	read(bitfd, &c, 1);
	bit = c & (1 << (id & 07));
	if (check && bit)
		warn("'bitfile' indicated you had news");
	c &= ~bit;
	lseek(bitfd, (long)(id>>3), 0);
	write(bitfd, &c, 1);
	close(bitfd);
	unlock();
}

/*
 * Update uindex, bitfile for article described at np.
 * Delete uindex entries which are not in /etc/passwd,
 * and add new entries for those who subscribe to the article.
 * Set bitfile bits for all users who subscribe to it.
 */
newart(np)
struct nrec *np;
{
	char pwbits[BTSZ];	/* uid bit map of /etc/passwd */
	char bitbuf[BTSZ];	/* incore bit file */
	register id_t i;
	register struct urec *urecp;
	register int bitfd, pwerrno;
	struct passwd *pw;
	time_t t;

	for (i = 0; i < BTSZ; i++)
		pwbits[i] = 0;

	/* scan /etc/passwd, set bits */
	errno = 0;	/* clear syscall error indicator */
	pwerrno = 1;	/* assume failure reading /etc/passwd */
	while ((pw = getpwent()) != NULL) {
		if ((unsigned)(i = pw->pw_uid) >= (unsigned)USIZE)
			lxerror("passwd uid out of range");
		pwbits[i>>3] |= (1<<(i&07));
		pwerrno = 0;	/* okay if at least one entry found */
	}
	endpwent();
	if (pwerrno == 0)
		pwerrno = errno;	/* save errno from passwd check */
	errno = 0;

	urecp = (struct urec *)xmalloc(sizeof(struct urec));
	lock();

	/* add article to .nindex */
	n_opena();
	n_write(np);
	n_close();
/*	t = atot("last month");*/
	t = np->n_date - (60L*60L*24L*30L);

	if ((bitfd = open(BITFILE, 2)) < 0)
		lxerror(BFERR);
	read(bitfd, bitbuf, BTSZ);

	/* scan uindex, delete entries no longer in /etc/passwd. */
	/* scan uindex, mark users who subscribe to this article, */
	/* and xor pwbits for users in uindex */
	u_openr();
	while (u_read(urecp)) {
		if (pwbits[urecp->u_uid>>3] & (1<<(urecp->u_uid&07))) {
			if (ngmatch(np->n_ings, urecp->u_sngs))
				bitbuf[urecp->u_uid>>3] |= (1<<(urecp->u_uid&07));
		}
		else
			bitbuf[urecp->u_uid>>3] &= ~(1<<(urecp->u_uid&07));
		pwbits[urecp->u_uid>>3] ^= (1<<(urecp->u_uid&07));
	}
	u_close();

	/* scan pwbits to see if uindex need be changed */
	for (i = 0; i < BTSZ; i++)
		if (pwbits[i])
			break;

	/* sync uindex if necessary, and if no errors reading passwd */
	if (i < BTSZ && pwerrno == 0) {
		u_openm();
		while (u_read(urecp)) {
#ifdef	PW_DEL
			if (pwbits[urecp->u_uid>>3] & (1<<(urecp->u_uid&07))
			 && urecp->u_date < t) {
				pwbits[urecp->u_uid>>3] &= ~(1<<(urecp->u_uid&07));
				continue;
			}
#endif
			u_write(urecp);
		}


#ifdef	PW_ADD
		/* scan usebits to append new users. */
		urecp->u_date = np->n_date-1;
		ngcat(strcpy(urecp->u_sngs, SNGDFLT));
		for (i = 0; i < USIZE; i++) {
			if (pwbits[i>>3] & (1<<(i&07))) {
				urecp->u_uid = i;
				u_write(urecp);
			}
		}
#endif
		u_close();
	}
	lseek(bitfd, 0L, 0);
	write(bitfd, bitbuf, BTSZ);
	close(bitfd);
	unlock();
	free((char *)urecp);
}

/*
 * Update bitfile for user described at up
 */
newsub(up)
struct urec *up;
{
	struct nrec nrec;
	register int bitfd;
	char bitbuf[BTSZ];	/* incore bit file */

	lock();
	if ((bitfd = open(BITFILE, 2)) < 0)
		lxerror(BFERR);
	read(bitfd, bitbuf, BTSZ);
	bitbuf[up->u_uid>>3] &= ~(1<<(up->u_uid&07));
	n_openl(up->u_date);
	while (n_read(&nrec)) {
		if (nrec.n_date <= up->u_date)
			continue;
		if (ngmatch(nrec.n_ings, up->u_sngs))
			bitbuf[up->u_uid>>3] |= (1<<(up->u_uid&07));
	}
	n_close();
	lseek(bitfd, 0L, 0);
	write(bitfd, bitbuf, BTSZ);
	close(bitfd);
	unlock();
}
