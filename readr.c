/*
 * Decide what the user wants to read
 * and print it out in the desired format.
 *
 * The user's last read time is updated if the user
 * did not specify any of -n, -t, -l, -r options.
 */

#include "param.h"
#define FMETA '%'

static char coptfile[] = "/tmp/newsXXXXXX";
static char NOFUT[] = "No news of the future yet";
static char WUSAGE[] = "usage: w filename\n";
static char FUSAGE[] = "usage: f [filename]\n";
static char ARTGONE[] = "Article is gone\n";
static char USAGE[] =
	"(n)ext re(p)rint (w)rite (q)uit (r)eply (c)ancel [+-][n]\n";
static char NOPAGER[] = "Pagination disabled\n";
static char INTERRUPT[] = "Interrupt\n";

static struct artlist *abase;	/* base pointer */
static struct artlist *freep;	/* pointer to next free slot */
static bool artfull;		/* true if artlist is full */

readr()
{
	register char *bptr;		/* temp pointer			*/
	register struct artlist *aptr;	/* pointer to artlist		*/
	struct artlist *oaptr;		/* old art pointer		*/
	register struct artlist *taptr;	/* temp art pointer		*/
	struct artlist *advance();
	register bool update;	/* true if must update last read time	*/
	bool qflag;		/* true if "q" reply given		*/
	bool badart;
	int can_err;		/* To catch infinite cancel loops	*/
	bool can_done;		/* To control running ninclean		*/
	int i;			/* handy integer			*/

	time_t rdate;		/* article must be newer than rdate	*/
	time_t bdate;		/* article must be older than bdate	*/
	time_t maxadate;	/* most recent article selected		*/
	time_t maxrdate;	/* most recent article read by user	*/
	time_t omaxrdate;	/* user's previous last read time	*/

	bool s_page;		/* true if should try to page		*/
	bool are_page;		/* true if currently running to pager	*/

	struct urec *urecp;
	struct nrec nrec;

	FILE *ofp;		/* for output of readr			*/
	FILE *fp, *tfp;		/* for use with h_read interface	*/
	struct hbuf h;		/* ditto.				*/

	h_init(&h);
	/* Allocate initial room for articles.  Avoid fragmentation. */
	abase = (struct artlist *)xmalloc(artsiz*sizeof(struct artlist));
	freep = abase;
	ofp = stdout;
	if (o_cflag && *header.h_control) {
		umask(0);
		mktemp(coptfile);	/* get "unique" file name */
		errno = 0;		/* reset error indicator */
		ofp = xfopen(coptfile, "w");
		umask(N_UMASK);
		o_cflag = FALSE;
		o_pflag = TRUE;
	}
	/* the following two kludges necessary because the
	 * new options processor in main won't support
	 * using '\0' as the filchar.
	 */
	if (o_tflag)
		for (bptr = titlebuf; *bptr != '\0'; bptr++)
			if (*bptr == ' ')
				*bptr = '\0';
	if (o_alflag)
		for (bptr = namebuf; *bptr != '\0'; bptr++)
			if (*bptr == ' ')
				*bptr = '\0';

	/* set up to use a pager */
	s_page = (!o_xflag && !o_pflag && !o_lflag && pager && *pager &&
		  ofp == stdout && fromtty && totty);
	are_page = FALSE;

	rdate = 0;	/* means beginning of time */
	bdate = 0;	/* means end of time */
	if (o_aflag && *adatebuf != '\0') {
		if ((rdate = atot(adatebuf)) < 0)
			xerror("Cannot parse -a date string");
		if (rdate > now+5)	/* +5 is kludge, since time marches */
			xerror(NOFUT);
	}
	if (o_bflag && *bdatebuf != '\0') {
		if ((bdate = atot(bdatebuf)) < 0)
			xerror("Cannot parse -b date string");
		if (bdate > now+5)
			xerror(NOFUT);
	}
	if (o_nflag)
		ngfcheck(TRUE);

	/* read uindex for the rest of the default info. */
	urecp = (struct urec *)xmalloc(sizeof(struct urec));
	u_openr();
	while (u_read(urecp))
		if (urecp->u_uid == uid) goto got_it;

	/* new user */
	urecp->u_date = now;
	ngcat(strcpy(urecp->u_sngs, SNGDFLT));
got_it:
	u_close();

	/* if no o_aflag or o_bflag, use user's last read time, current time */
	if (!o_aflag && !o_bflag) {
		rdate = urecp->u_date;
/*		bdate = now;*/
	}

	 /* If no o_nflag, use user's sublist. */
	if (!o_nflag)
		dstrcpy(&header.h_ngs, urecp->u_sngs);

	/* save user's old last read time, then release urec */
	omaxrdate = urecp->u_date;
	free((char *)urecp);

	update = !o_nflag && !o_tflag && !o_lflag && !o_rflag && !o_alflag;
	if (o_aflag && rdate)
		update = FALSE;
	qflag = FALSE;
	maxadate = 0;

	fp = NULL;
	oaptr = NULL;

one_more_time:
	/* setup artlist */
	i = 0;	/* count new found articles */
	artfull = FALSE;
	if (o_alflag) {
		/* use the explicitly passed list of articles. */
		badart = FALSE;
		bptr = namebuf;
		while (*bptr) {
			if (!isarticle(bptr)) {
				printf("Bad article name %s.\n", bptr);
				badart = TRUE;
			}
			else if ((tfp = ah_read(&h, bptr)) == NULL) {
				printf("Article %s %s.\n", bptr,
					afilelen(bptr)==0? "has been cancelled": "not found");
				badart = TRUE;
			}
			else {
				xfclose(tfp);
				if (addart(h.h_name, atot(h.h_rdate)))
					i++;
			}
			while (*bptr++);
		}
		if (badart) {
			printf("\n");
			fflush(stdout);
		}
	}
	else {
		/* generate the list of articles to read */
		n_openl(rdate);
		while (!sigtrap && n_read(&nrec)) {

			/* check the date. */
			if (nrec.n_date <= rdate)
				continue;
			if (bdate && nrec.n_date >= bdate)
				continue;

			/* check the newsgroups. */
			if (!ngmatch(nrec.n_ings, header.h_ngs))
				continue;

			/* check the title matching. */
			if (o_tflag && !titmat(nrec.n_name, titlebuf))
				continue;

			if (addart(nrec.n_name, nrec.n_date))
				i++;
			if (nrec.n_date > maxadate)
				maxadate = nrec.n_date;
		}
		n_close();
	}

	if (o_rflag) {
		revart(abase, freep);
		if (artfull) {
			revart(freep, &abase[artsiz]);
			freep = &abase[artsiz];
		}
	}
	if (sigtrap) {
		cdump(ofp);
		goto goret;
	}

	/* if no news, or 'quit' was requested, update the user */
	if (i == 0 || qflag) {
		if (maxadate == 0) {
			fprintf(stderr, "No news.\n");
			if (update && !bdate)
				nonews(uid, TRUE);
			cdump(ofp);
			goto goret;
		}
		maxrdate = 0;
		for (aptr = abase; aptr < freep; aptr++) {
			if ((aptr->a_status&A_READ)
			&&  aptr->a_date > maxrdate)
				maxrdate = aptr->a_date;
		}
		if (update && maxrdate > omaxrdate) {
			lock();
			u_update(maxrdate);
			if (maxrdate == maxadate && !bdate)
				nonews(uid, FALSE);
			unlock();
		}
		cout(ofp);
		goto goret;
	}
	rdate = maxadate;
	aptr = freep-i;


	/* loop reading articles. */
	can_err = 0;
	can_done = FALSE;
	for (;;) {
		if (o_xflag && sigtrap)
			goodbye();
		aptr = advance(aptr, 0);
		if (aptr != oaptr) {
			if (fp != NULL) {
				if (!o_xflag && !o_lflag && oaptr
				&&  advance(oaptr, 1) == aptr) {
					if (s_page && !are_page &&
					    h.h_nlines > PAGELC) {
						are_page = TRUE;
						fflush(stdout);
						ofp = upopen(pager, TRUE);
					}
					tprint(fp, ofp);
					fflush(ofp); /* so SIGINT is better */
					oaptr->a_status |= A_READ;
					oaptr->a_status &= ~A_INTR;
				}
				xfclose(fp);
				fp = NULL;
				if (are_page) {
					are_page = FALSE;
					if (npclose(ofp, TRUE) && !sigtrap) {
						s_page = FALSE;
						aptr = oaptr;
						fprintf(stderr, NOPAGER);
					}
					ofp = stdout;
				}
			}
			if (aptr >= freep)
				break;
			if ((fp = ah_read(&h, aptr->a_name)) == NULL) {
				aptr->a_status &= ~A_EXIST;
				if (sigtrap)
					fprintf(stderr, "Be patient\n");
				sigtrap = FALSE;
				continue;
			}
			if (s_page && !o_cflag && h.h_nlines > PAGELC) {
				are_page = TRUE;
				fflush(stdout);
				ofp = upopen(pager, TRUE);
			}
			if (!o_xflag)
				h_print(&h, o_lflag, ofp);
			if (!o_cflag && !o_lflag) {
				if (!o_xflag)
					tprint(fp, ofp);
				aptr->a_status |= A_READ;
				aptr->a_status &= ~A_INTR;
				xfclose(fp);
				fp = NULL;
				if (are_page) {
					are_page = FALSE;
					if (npclose(ofp, TRUE) && !sigtrap) {
						s_page = FALSE;
						fprintf(stderr, NOPAGER);
						oaptr = NULL;
						ofp = stdout;
						continue;
					}
					ofp = stdout;
				}
			}
			oaptr = aptr;
		}
		if (o_lflag || o_pflag) {
			if (sigtrap) {
				qfflush(ofp);
				fprintf(ofp, "\n");
				cdump(ofp);
				_exit(0); /* kludge! drop when qfflush works */
				goto goret;
			}
			aptr++;
			continue;
		}
		for (;;) {
			if (sigtrap) {
				fprintf(stderr, INTERRUPT);
				aptr->a_status |= A_INTR;
			}
			sigtrap = FALSE;
			bptr = lbfr;
			*bptr = '\0';
			if (o_xflag) {
				if (xbuf)
					strcpy(bptr, xbuf);
				break;
			}
			fprintf(ofp, "%d of %d.  ", aptr+1-abase, freep-abase);
			if (o_cflag)
				fprintf(ofp, "(%d lines) More? [ynq] ",
						h.h_nlines);
			else
				fprintf(ofp, "? ");
			fflush(ofp);
			if (fgets(bptr, BUFLEN, stdin) != NULL)
				break;
			if (!sigtrap) {
				strcpy(bptr, "q\n");
				break;
			}
		}
		nstrip(bptr);
		while (*bptr == ' ' || *bptr == '\t')
			bptr++;
		switch (*bptr++) {

		/* get next article */
		case 'n':
		case 'd':
			if (*bptr != '\0') goto badropt;
			if (o_cflag) {
				xfclose(fp);
				fp = NULL;
				fprintf(ofp, "\n");
			}
			aptr->a_status |= A_READ;
			aptr++;
			break;

		case 'y':
			if (*bptr != '\0') goto badropt;
		case '\0':
			aptr++;
			break;

		/* reprint the article */
		case 'p':
			if (*bptr != '\0') goto badropt;
			oaptr = NULL;
			break;

		/* write out the article someplace */
		case 'w':
			taptr = aptr;
			if (*bptr == '-') {
				bptr++;
				taptr = advance(taptr, -1);
			}
			if (*bptr != '\0' && *bptr != ' ' && *bptr != '|') {
				fprintf(ofp, WUSAGE);
				break;
			}
			while (*bptr == ' ')
				bptr++;
			if (*bptr == '\0') {
				fprintf(ofp, WUSAGE);
				break;
			}
			fwsubr(save, taptr->a_name, bptr);
			break;

		/* back up some distance. */
		case '-':
		/* skip forwards */
		case '+':
			if (*bptr == '\0')
				strcat(bptr, "1");
			aptr = advance(aptr, atoi(bptr-1));
			oaptr = NULL;
			break;

		/* Go to end of list of articles */
		case '$':
			if (*bptr != '\0')
				goto badropt;
			aptr = freep-1;
			oaptr = NULL;
			break;

		/* exit - time updated to that of most recently read article */
		case 'q':
			if (*bptr != '\0') goto badropt;
			if (aptr->a_status&A_INTR)
				aptr->a_status &= ~A_READ;
			qflag = TRUE;
			if (update)
				goto one_more_time;
			goto goret;

		/* exit - no time update. */
		case 'x':
			if (*bptr != '\0') goto badropt;
			goto goret;

		/* cancel the article. */
		case 'c':
			if (*bptr != '\0') goto badropt;
			if (cancel(aptr->a_name)) {
				can_err = 0;
				can_done = TRUE;
				aptr->a_status |=  A_CAND;
				aptr->a_status &= ~A_EXIST;
				oaptr = NULL;
			}
			else {
				printf("Not cancelled.\n");
				if (++can_err > 50)
					lxerror("Infinite cancel loop?");
			}
			break;

		/* escape to shell */
		case '!':
			fwsubr(ushell, bptr, (char *)NULL);
			fprintf(ofp, "!\n");
			break;

		/* mail reply */
		case 'r':
			taptr = aptr;
			if (*bptr == '-') {
				bptr++;
				taptr = advance(taptr, -1);
			}
			if (*bptr != '\0') goto badropt;
			if ((tfp = ah_read(&h, taptr->a_name)) == NULL) {
				printf(ARTGONE);
				break;
			}
			xfclose(tfp);
			fixpath(h.h_from);
#ifdef SMAILER
			dfixtitle(&h.h_title, h.h_name);
			sprintf(bfr, "%s -s %s %s", SMAILER, h.h_title, h.h_from);
#else
			sprintf(bfr, "%s '%s'", NMAILER, h.h_from);
#endif
			fprintf(ofp, "%s\n", bfr);
			fwsubr(ushell, bfr, (char *)NULL);
			break;

		/* followup news article */
		case 'f':
			taptr = aptr;
			if (*bptr == '-') {
				bptr++;
				taptr = advance(taptr, -1);
			}
			if (*bptr != '\0' && *bptr != ' ') {
				fprintf(ofp, FUSAGE);
				break;
			}
			while (*bptr == ' ')
				bptr++;
			if ((tfp = ah_read(&h, taptr->a_name)) == NULL) {
				printf(ARTGONE);
				break;
			}
			xfclose(tfp);
			ngdel(h.h_ngs);
			dfixtitle(&h.h_title, (char *)NULL);
			dfixrefs(&h.h_refs, h.h_name);
			sprintf(bfr, "news -i %.80s -n %.80s -f %.80s",
				h.h_title, h.h_ngs, h.h_refs);
			if (*bptr) {
				fprintf(ofp, "%s < %s\n", bfr, bptr);
				fwsubr(ushell, bfr, bptr);
			}
			else {
				fprintf(ofp, "%s\n", bfr);
				fwsubr(ushell, bfr, (char *)NULL);
			}
			break;

		/* send to some system */
		case 'X':
			if (*bptr != '\0' && *bptr != ' ') {
				fprintf(ofp, "Bad system name.\n");
				break;
			}
			while (*bptr == ' ')
				bptr++;
			if (*bptr == '\0') {
				fprintf(ofp, "Missing system name.\n");
				break;
			}
			if (!sysxmit(bptr, aptr->a_name))
				fprintf(stderr, "%s not in SYSFILE\n", bptr);
			break;

		/* absolute move? */
		default:
			bptr--;
			if (i = atoi(bptr)) {
				aptr = &abase[i-1];
				oaptr = NULL;
				break;
			}
			goto badropt;

		case '?':
		/* error */
		badropt:
			fprintf(ofp, USAGE);
			break;
		}
	}

	/*
	 * Done with this batch of articles.
	 * Now pick up any that have been submitted
	 * while we were reading these.
	 */
	sigtrap = FALSE;
	if (update)
		goto one_more_time;
	cout(ofp);

goret:
	h_free(&h);
	h_free(&header);
	/*
	 * Only run 'ninclean' here if at least one article was cancelled.
	 * (Or, as a precaution, if article list filled up.)
	 * Otherwise, on systems with huge nindex files,
	 * multiple ninclean-s could start running, ruining response time.
	 */
	if (can_done || artfull)
		ninclean(abase, freep);
	free((char *)abase);
}

/*
 * Add to artlist the article with name 'name' and (local) date 't'.
 * Assumes articles are presented in ascending time order.
 */
addart(name, t)
char *name;
time_t t;
{
	register int rc;

	rc = TRUE;
	if (freep >= &abase[artsiz]) {
		if (artsiz >= ARTSIZ) {
			rc = FALSE;
			if (!artfull) {
				sprintf(bfr, "only the %d %s requested articles will be processed",
					ARTSIZ, o_rflag? "newest": "oldest");
				warn(bfr);
				artfull = TRUE;
			}
			if (!o_rflag)
				return(rc);
			/* let articles 'wrap around'.  */
			freep = abase;
		}
		else {
			abase=(struct artlist *)
			xrealloc((char *)abase, ARTSIZ*sizeof(struct artlist));
			freep = &abase[artsiz];
			artsiz = ARTSIZ;
		}
	}

	strcpy(freep->a_name, name);
	freep->a_date = t;
	freep->a_status = A_EXIST;
	freep++;
	return(rc);
}

/*
 * Match title.
 */
titmat(file, titlist)
register char *file, *titlist;
{
	struct hbuf h;
	register char *p;
	register FILE *fp;
	register int titlen;

	h_init(&h);
	if ((fp = ah_read(&h, file)) == NULL) {
		h_free(&h);
		return(FALSE);
	}
	xfclose(fp);

	while (*titlist != '\0') {
		titlen = strlen(titlist);
		for (p = h.h_title; *p != '\0'; p++)
			if (strncmp(p, titlist, titlen) == 0) {
				h_free(&h);
				return(TRUE);
			}
		titlist += titlen+1;
	}
	h_free(&h);
	return(FALSE);
}

/*
 * Save the news item in the user's file.
 */
save(file, to)
register char *file, *to;
{
	register FILE *ufp, *hfp;
	struct hbuf h;
	bool append;

	h_init(&h);
	if ((hfp = ah_read(&h, file)) == NULL) {
		printf(ARTGONE);
		h_free(&h);
		return(0);
	}
	revert();
	if (index(to, '&')) {
		printf("& not allowed\n");
		h_free(&h);
		return(0);
	}
	if (*to == '|')
		ufp = upopen(&to[1], FALSE);
	else {
		append = filelen(to) > 0;
		ufp = fopen(to, "a");
	}
	if (ufp == NULL) {
		printf("Cannot append to %s\n", to);
		h_free(&h);
		return(0);
	}
	h_print(&h, FALSE, ufp);
	tprint(hfp, ufp);
	xfclose(hfp);
	if (*to == '|')
		npclose(ufp, TRUE);
	else {
		xfclose(ufp);
		if (append)
			printf("Appended\n");
		else
			printf("New file\n");
	}
	h_free(&h);
	return(0);
}

/*
 * Print out the rest of the article.
 */
tprint(ifp, ofp)
register FILE *ifp, *ofp;
{
	register int c;

	while ((c = getc(ifp)) != EOF && !sigtrap)
		putc(c, ofp);
	if (sigtrap)
		qfflush(ofp);
	fflush(ofp);
	fprintf(ofp, (sigtrap? "\n\n" : "\n"));
}

/*
 * advance aptr by indicated amount, skipping cancelled items.
 * new aptr is returned, which might be >= freep.
 */
struct artlist *
advance(aptr, n)
register struct artlist *aptr;
register int n;
{
	register int incr;

	incr = 1;
	if (n < 0)
		incr = -1;
	for (;;) {
		/* before start of artlist? */
		if (aptr < abase) {
			aptr = abase;
			n = 0;
			incr = 1;
		}
		/* past end of artlist? */
		if (aptr >= freep)
			break;
		/* as long as this article is bad, advance */
		if ((aptr->a_status&A_EXIST) == 0) {
			aptr += incr;
			continue;
		}
		/* check n to decide whether to continue, or leave */
		if (n == 0)
			break;
		aptr += incr;
		n -= incr;
	}
	return(aptr);
}

/*
 * reverse artlist entries from ap to bp
 */
revart(ap, bp)
register struct artlist *ap, *bp;
{
	struct artlist ta;

	while (ap < bp) {
		ta = *ap;
		*ap++ = *--bp;
		*bp = ta;
	}
}

/*
 * If ofp != stdout, close it and run the script in header.h_control.
 */
cout(ofp)
FILE *ofp;
{
	register char *p, *q, *r;

	if (ofp == stdout)
		return;
	xfclose(ofp);
	p = header.h_control;
	q = lbfr;
	while ((*q = *p++) != '\0')
		if (*q++ == FMETA) {
			q--;
			r = coptfile;
			while ((*q++ = *r++) != '\0')
				;
			q--;
		}
	fwsubr(ushell, lbfr, (char *)NULL);
	unlink(coptfile);
}

cdump(ofp)
register FILE *ofp;
{
	if (ofp == stdout)
		return;
	xfclose(ofp);
	unlink(coptfile);
}

/*
 * Quiet 'flush'.
 * Empty (without fflush()) the buffer for stream fp.
 */
/* ARGSUSED */
qfflush(fp)
FILE *fp;
{
	/* Alas, stdio does not permit this */
}


/*
 * Add leading "Re: artname:" to t, if not present,
 * and quote the string with single quotes.
 * The "artname" business is necessary for stupid mail programs.
 */
static
dfixtitle(t, artname)
register char **t, *artname;
{
	register char *p;

	p = *t;
	if (*p++ == 'R' && *p++ == 'e' && *p++ == ':') {
		if (artname == NULL)
			goto doquote;
		while (*p)
			if (*p++ == ':')
				goto doquote;
	}
	if (artname)
		sprintf(bfr, "Re: %.80s: %.80s", artname, *t);
	else
		sprintf(bfr, "Re: %.80s", *t);
	dstrcpy(t, bfr);
doquote:
	dquote(t);
}

/*
 * pre-pend artname to refs
 */
dfixrefs(r, artname)
register char **r, *artname;
{
	if (*r == NULL)
		dstrcpy(r, artname);
	else {
		sprintf(bfr, "%s %s", artname, *r);
		dstrcpy(r, bfr);
	}
	dquote(r);
}

/*
 * Truncate return path at first space character.
 * This is necessary for stupid mailers.
 */
fixpath(p)
register char *p;
{
	if (p = index(p, ' '))
		*p = '\0';
}

/*
 * Totally single-quote the string *t.
 */
dquote(t)
register char **t;
{
	register char *p, *q;

	q = bfr;
	*q++ = '\'';
	for (p = *t; *p;) {
		if (q >= &bfr[MAXLEN-8])
			lxerror("quote overflow");
		if (*p == '\'') {
			*q++ = '\'';
			*q++ = '"';
			*q++ = '\'';
			*q++ = '"';
		}
		*q++ = *p++;
	}
	*q++ = '\'';
	*q = '\0';
	dstrcpy(t, bfr);
}
