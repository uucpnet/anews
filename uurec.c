#include <stdio.h>

/*
 * Process a news article which has been shipped via /bin/mail.
 */

#define FROM	01
#define NLIN	02
#define OTHER	03

#define SKIPPING	010
#define READING		020

#define BFSZ 250

#define TRUE	1
#define FALSE	0

#define EOT	'\004'

extern	char	*strcat(), *strcpy();
extern	char	*frombreak();
extern	FILE	*popen();

/* ARGSUSED */
main(argc, argv)
int argc;
char **argv;
{
	int debug = 0;
	char buf[BFSZ], fbuf[BFSZ];
	register char *p;
	register FILE *pipe;
	register int mode, frmflg, pathcnt;

	mode = SKIPPING;
	frmflg = FALSE;
	while (fgets(buf, BFSZ, stdin) != NULL) {
		if (debug)
			printf("Mode = %d\nType = %d\nBuf = %s\n\n",
				mode, type(buf), buf);
		switch (mode | type(buf)) {

		case FROM | SKIPPING:
			mode = READING;
			frmflg = TRUE;
			p = fbuf;
			break;

		case FROM | READING:
			if (!frmflg) {
				frmflg = TRUE;
				p = fbuf;
				pclose(pipe);
				pathcnt = 0;
			}
			p = frombreak(p, buf);
			break;

		case NLIN | SKIPPING:
			break;

		case NLIN | READING:
			if (frmflg) {
				frmflg = FALSE;
				--p;
				while (p >= fbuf && *--p != '!')
					;
				*++p = '\0';
				pathcnt = 0;
				if ((pipe = popen("rnews", "w")) == NULL) {
					fprintf(stderr, "pipe failed.\n");
					exit(1);
				}
			}
			if (++pathcnt == 3)
				fputs(fbuf, pipe);
			fputs(buf+1, pipe);
			break;

		case OTHER | SKIPPING:
			break;

		case OTHER | READING:
			if (pathcnt == 0)
				break;
			pclose(pipe);
			pathcnt = 0;
			mode = SKIPPING;
		}
	}
}

type(p)
register char *p;
{
	while (*p == ' ' || *p == '?')
		++p;

	if (*p == 'N')
		return (NLIN);

	if (strncmp(p, ">From", 5) == 0)
		return (FROM);

	if (strncmp(p, "From", 4) == 0)
		return (FROM);

	return(OTHER);
}

/*
 * Get the system name out of a from line.
 */
char *
frombreak(buf, fbuf)
register char *buf, *fbuf;
{
	register char *p;

	/* break the line into tokens. */
	p = fbuf;
	while (*++p != '\0')
		switch (*p) {
		case '\n':
		case '\t':
		case ' ':
			*p = '\0';
			break;
		case EOT:
			goto garbled;
		default:;
		}
	*++p = EOT;
	*++p = '\0';

	for (p=fbuf; *p != EOT  || p[1] != '\0'; p += strlen(p)+1) {
		if (strcmp(p, "forwarded") == 0)
			return(buf);
		if (strcmp(p, "remote") == 0) {
			p += strlen(p)+1;
			if (strcmp(p, "from") == 0) {
				p += strlen(p)+1;
				strcpy(buf, p);
				strcat(buf, "!");
				return(buf+strlen(buf));
			}
		}
	}
    garbled:
	strcat(buf, "???!");
	return(buf+4);
}
