#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <pwd.h>
#include "local.h"

#define	BUFLEN	120	/* length of longest normal buffer needed	*/
#define	MAXLEN	2000	/* length of longest buffer needed		*/
#define	DATELEN	35	/* length of longest allowed date string	*/
#define	NAMELEN	20	/* length of longest allowed file name		*/

#define	SNLN	8	/* max significant characters in sysname	*/
#define	NGDELIM	','	/* delimit character in news group line		*/
#define	N_UMASK 022	/* mask for umask call				*/
#define PAGELC	20	/* Number of lines on a screen			*/

#define	TRUE	1
#define	FALSE	0

typedef	int	bool;
typedef	unsigned int	MTYPE;		/* type of mem allocator arg	*/
typedef	double * MALIGN;		/* type returned by mem allocators */
typedef	unsigned int	id_t;		/* type of user-ids	*/

struct	hbuf {
	char	*h_from;		/* source string	*/
	char	h_adate[DATELEN];	/* date submitted	*/
	char	*h_title;		/* title		*/
	char	h_name[NAMELEN+1];	/* unique name		*/
	char	*h_ngs;			/* newsgroup line	*/
	char	*h_reply;		/* reply to addr	*/
	char	h_rdate[DATELEN];	/* date received	*/
	char	*h_edate;		/* expiration date	*/
	short	h_nlines;		/* length in lines	*/
	char	*h_control;		/* control message	*/
	char	*h_refs;		/* arts ref'd by this one*/
	char	*h_path;		/* pseudo return path	*/
	struct	hbuf_opts {		/* list of opt fields	*/
		struct hbuf_opts *ho_l;	   /* list pointer	*/
		char   *ho_p;		   /* text of option	*/
	}	*h_options;		/* list head.		*/
};

struct	nrec {
	char	n_name[NAMELEN+1];	/* unique name		*/
	time_t	n_date;			/* date submitted	*/
	char	n_ings[BUFLEN];		/* newsgroup line	*/
};

struct	urec {
	id_t	u_uid;			/* user i-d		*/
	time_t	u_date;			/* last read time	*/
	char	u_sngs[MAXLEN];		/* subscription list	*/
};

#define	S_AFLAG	001	/* Transmit in 'A' format		*/
#define	S_BFLAG	002	/* Transmit in 'B' format		*/
#define	S_NFLAG	004	/* Use mail handshakes (not implemented)*/

struct	srec {
	char	s_name[NAMELEN+1];	/* system name		*/
	char	s_sngs[MAXLEN];		/* system subscriptions */
	short	s_flags;		/* transmission flags	*/
	char	s_xmit[BUFLEN];		/* system xmit routine	*/
};

#define	A_EXIST	001	/* article exists, as far as we know */
#define	A_READ	002	/* article has been read */
#define	A_CAND	004	/* article has been cancelled */
#define	A_INTR	010	/* article printing was interrupted */

struct artlist {
	char	a_name[NAMELEN+1];
	char	a_status;
	time_t	a_date;
};

extern	int	errno;
extern	char	**environ;

extern	char	*cmdname;

extern	id_t	uid, gid, duid, dgid;
extern	int	savmask;
extern	char	**savenv;
extern	unsigned artsiz;
extern	char	*pager, *editor;
extern	bool	sigtrap, bitup, rnews, fromtty, totty;
extern	bool	o_lflag, o_pflag, o_rflag, o_tflag, o_iflag,
		o_aflag, o_bflag, o_eflag, o_nflag, o_sflag, o_expflag,
		o_cflag, o_alflag, o_refflag, o_xflag;

extern	time_t	now;
extern	struct	hbuf header;
extern	char	bfr[MAXLEN], lbfr[2*BUFLEN];
extern	char	NULLSTR[];
extern	char	*username, *userhome;
extern	char	*adatebuf, *bdatebuf, *titlebuf, *namebuf, *xbuf;

extern	char	NEWSD[], ARTD[];
extern	char	CAND[];

extern	char	LOCKFILE[];
extern	char	BITFILE[];
extern	char	SEQFILE[];
extern	char	NGFILE[];
extern	char	UINDEX[], NINDEX[];
extern	char	SYSFILE[];
extern	char	TUFILE[], TNFILE[];
extern	char	LOGFILE[], SUBLIST[];

extern	char	SYSNAME[];
extern	char	NEWSU[], NEWSG[];

extern	char	SNGDFLT[], SNGMUST[], INGDFLT[];
extern	char	uudead[], uucancel[];
#ifdef CTL_MSG
extern	char	CTLGROUP[];
#endif
extern	char	PARTIAL[];
extern	char	NETCHRS[];

extern	FILE	*xfopen(), *h_read(), *ah_read();
extern	FILE	*mail_open(), *upopen();
extern	char	*strcpy(), *strncpy(), *strcat(), *index();
extern	char	**dstrcpy(), **dstrcat(), *space(), *nonspace();
extern	char	*mktemp(), *nmktemp(), *mknname(), *mkaname();
extern	char	*malloc(), *realloc(), *ctime(), *getenv();
extern	char	*sitename(), *sname();
extern	MALIGN	xmalloc(), xrealloc();
extern	struct	passwd *getpwnam(), *getpwuid(), *getpwent();
extern	int	broadcast(), save(), newssave(), ushell(), nshell(), xshell(),
		onsig();
extern	long	atol(), lseek();
extern	off_t	filelen(), afilelen();
extern	time_t	atot(), time();
#ifdef	GETDATE
extern	time_t	getdate();
#endif
extern	bool	ctl(), control();
