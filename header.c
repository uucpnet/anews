#include "param.h"

/*
 * This file contains the header manipulation routines.
 */

#define	PROTO	'A'		/* 'A' format protocol name		*/

/*
 * Switch to get to routines to process headers.
 */
#define	HEAD_U	0	/* dummy routine, returns NULL			*/
FILE	*h_uread();
#define	HEAD_A	1	/* Original 'A' news format			*/
FILE	*h_aread();
#define HEAD_B	2	/* Berkeley's 'B' news format			*/
FILE	*h_bread();

/*
 * Type B header parsing table:
 */
#define OTHER	0
#define	FROM	1
#define	NGS	2
#define	TITLE	3
#define	SDATE	4
#define	RDATE	5
#define	EDATE	6
#define	ARTID	7
#define REPLY	8
#define NLINES	9
#define	CONTROL	10
#define	REFER	11
#define	PATH	12
/* not yet
#define	DISTRIB	13
#define	MSGID	14
#define	RELAYV	15
#define	POSTV	16
#define	ORGANIZ	17
*/

struct hb_tab {
	char *hb_s;
	int  hb_t;
} hb_tab[] = {
	/* must be in lower case*/
	"from",		FROM,
	"to",		NGS,
	"newsgroups",	NGS,
	"subject",	TITLE,
	"title",	TITLE,
	"date",		SDATE,
	"posted",	SDATE,
	"received",	RDATE,
	"expires",	EDATE,
	"article-i.d.",	ARTID,
	"reply-to",	REPLY,
	"lines",	NLINES,
	"control",	CONTROL,
	"references",	REFER,
	"path",		PATH,
/* not yet
	"relay-version",	RELAYV,
	"posting-version",	POSTV,
	"message-id",	MSGID,
	"distribution",	DISTRIB,
	"organization",	ORGANIZ,
*/
	0,		0,
};

/*
 * Read header from file NEWSD/name into *hp.
 * Return (FILE *) if header okay, else NULL.
 */
FILE *
ah_read(hp, name)
register struct hbuf *hp;
register char *name;
{
	mkaname(bfr, name);
	return(h_read(hp, bfr));
}

/*
 * Read header from file name into *hp.
 * Return (FILE *) if header okay, else NULL.
 */
FILE *
h_read(hp, name)
register struct hbuf *hp;
char *name;
{
	register FILE *fp, *rfp;

	if ((fp = fopen(name, "r")) == NULL)
		return(NULL);
	h_free(hp);
	fgets(bfr, MAXLEN, fp);
	if (!nstrip(bfr)) {
		xfclose(fp);
		return(NULL);
	}

	switch(h_type(bfr)) {
	case HEAD_U:
		rfp = h_uread(hp, fp);
		break;
	case HEAD_A:
		rfp = h_aread(hp, fp);
		break;
	case HEAD_B:
		rfp = h_bread(hp, fp);
		break;
	default:
		rfp = NULL;
		break;
	}
	if (rfp == NULL) {
		xfclose(fp);
		return(NULL);
	}
	return(rfp);
}

/*
 * Determine the header type from the first
 * line of the header.
 * All formats are supposed to be distinguishable by
 * their first character, but...
 */
h_type(s)
register char *s;
{
	bool hb_cntl();

	if (hb_cntl(s))
		return(HEAD_B);
	else switch(*s) {
		case PROTO:
			return(HEAD_A);
		default:
			return(HEAD_U);
	}
	/*NOTREACHED*/
}

/*
 * Return true if passed a legal 'B' format control line.
 * The definition here is almost rfc733.  Only departure
 * (that I know of) is the 'From ' kludge.
 */
bool
hb_cntl(s)
register char *s;
{
	if (*s == ' ' || *s == '\t' || *s == ':')
		return(FALSE);
	/* Kludge for bad 'From' lines.  Not sure why this is needed. */
	if (s[0] == 'F' && s[1] == 'r' && s[2] == 'o' && s[3] == 'm' &&
	    s[4] == ' ')
		return(TRUE);
	if (index(s, ':'))
		return(TRUE);
	return(FALSE);
}

/*
 * Dummy routine.  Used to process error exit from h_type.
 */
/*ARGSUSED*/
FILE *
h_uread(hp, fp)
struct hbuf *hp;
FILE *fp;
{
	return(NULL);
}

/*
 * Read a type 'A' header from stream fp into hp.
 * The first line of the header is assumed to be in bfr.
 * Return fp if header ok, else NULL.
 */
FILE *
h_aread(hp, fp)
register struct hbuf *hp;
register FILE *fp;
{
	if (strlen(bfr+1) > NAMELEN)
		return(NULL);
	strncpy(hp->h_name, bfr+1, NAMELEN+1);	/* file name */
	fgets(bfr, BUFLEN, fp);	/* newsgroup list */
	if (!nstrip(bfr) || strlen(bfr) > BUFLEN-30)
		return(NULL);
	dngcat(dstrcpy(&hp->h_ngs, bfr));
	fgets(bfr, MAXLEN, fp);	/* source path */
	if (!nstrip(bfr))
		return(NULL);
	dstrcpy(&hp->h_from, bfr);
	/* strip off sys! from front of path. */
	if (islocal(hp->h_from))
		strcpy(hp->h_from, hp->h_from+strlen(SYSNAME)+1);
	fgets(hp->h_adate, DATELEN, fp);	/* date */
	if (!nstrip(hp->h_adate))
		return(NULL);
	fgets(bfr, BUFLEN, fp);	/* title */
	if (!nstrip(bfr))
		return(NULL);
	dstrcpy(&hp->h_title, bfr);
	dfree(&hp->h_reply);
	hp->h_rdate[0] = '\0';
	hp->h_nlines = linecnt(fp);
	return(fp);
}

/*
 * Read a type 'B' header from stream fp into hp.
 * The first line of the header is assumed to be in bfr.
 * Return fp if header ok, else NULL.
 *
 * This routine will discard the first nonheader line
 * of the article.  Therefor, it is a good idea to
 * have a blank line separating the header from the body
 */
FILE *
h_bread(hp, fp)
register struct hbuf *hp;
register FILE *fp;
{
	register int type, len;
	char *field;
	struct hbuf_opts **hp_op;
	bool nlf;	/* true if nlines field encountered */
	int c;
	int f_have;
	/* list of required fields */
	int f_need = (1<<FROM)|(1<<SDATE)|(1<<TITLE)|(1<<ARTID)|(1<<NGS);

	f_have = 0;
	nlf = FALSE;
	hp->h_nlines = 0;
	hp_op = &hp->h_options;
	while (hb_cntl(bfr)) {
		/* first off, process continuation lines (!) */
		field = bfr;
		len = 0;
		for (;;) {
/*			hb_canon(field);*/
			len += strlen(field);
			field = bfr + len;
			if ((c = getc(fp)) == EOF)
				return(NULL);
			ungetc(c, fp);
			if (c != ' ' && c != '\t')
				break;
			fgets(field, sizeof(bfr) - len, fp);
			if (!nstrip(field))
				return(NULL);
		}
		type = hb_type(bfr, &field);
		f_have |= (1<<type);
		switch (type) {
		case FROM:
			dstrcpy(&hp->h_from, field);
			/* strip off sys! from front of path. */
			if (islocal(hp->h_from))
				strcpy(hp->h_from, hp->h_from+strlen(SYSNAME)+1);
			break;
		case NGS:
			dngcat(dstrcpy(&hp->h_ngs, field));
			break;
		case TITLE:
			dstrcpy(&hp->h_title, field);
			break;
		case SDATE:
			strncpy(hp->h_adate, field, DATELEN);
			break;
		case RDATE:
			strncpy(hp->h_rdate, field, DATELEN);
			break;
		case EDATE:
			dstrcpy(&hp->h_edate, field);
			break;
		case ARTID:
			if (strlen(field) > NAMELEN)
				return(NULL);
			strncpy(hp->h_name, field, NAMELEN+1);
			break;
		case REPLY:
			dstrcpy(&hp->h_reply, field);
			break;
		case NLINES:
			hp->h_nlines = atoi(field);
			nlf = TRUE;
			break;
		case CONTROL:
			dstrcpy(&hp->h_control, field);
			break;
		case REFER:
			dstrcpy(&hp->h_refs, field);
			break;
		case PATH:
			dstrcpy(&hp->h_path, field);
			/* clean up here? */
			/* kludge! */
		case OTHER:
		default:
			*hp_op = (struct hbuf_opts *)xmalloc(
				  sizeof (struct hbuf_opts));
			(*hp_op)->ho_l = NULL;
			(*hp_op)->ho_p = NULL;
			dstrcpy( & ((*hp_op)->ho_p), bfr);
			hp_op = & ((*hp_op)->ho_l);
			if (!nlf)
				hp->h_nlines++;
			break;
		}
		fgets(bfr, MAXLEN, fp);
		if (!nstrip(bfr))
			return(NULL);
	}
	if ( (f_have & f_need) != f_need)
		return(NULL);
	return(fp);
}

/*
 * Canonicalize a control line according to RFC-733.
 * This involves converting all runs of white space to
 * a single space and removing all chars with ascii value
 * less than space.
 *
 * Canonicalization is done only on the control field
 * (up to the first colon). After the colon, all leading
 * white space is discarded, and all subsequent control
 * characters are discarded.
 */
hb_canon(s)
register char *s;
{
	register char *p;

	p = s;
	while (*s && *s != ':') {
		while (*s && *s != ':' && *s != ' ' && *s != '\t')
			if (*s > ' ')
				*p++ = *s++;
			else
				s++;
		if (!(*s && *s != ':'))
			break;
		*p++ = ' ';
		while (*s && *s <= ' ')
			s++;
	}
	if (*s++) {
		*p++ = ':';
		while (*s && *s <= ' ')
			s++;
		while (*s)
			if (*s >= ' ' || *s == '\t')
				*p++ = *s++;
			else
				s++;
	}
	*p = '\0';
}

/*
 * Parse a format 'B' control line.
 * Returns type of control line and pointer
 * to rest of line. The 'rest of line' pointer
 * is not valid when the returned type is OTHER.
 */
hb_type(s, field)
register char *s;
char **field;
{
	register char *p, *q;
	register struct hb_tab *h;

	/* Kludge for bad 'From' lines.  Not sure why this is needed. */
	if (s[0] == 'F' && s[1] == 'r' && s[2] == 'o' && s[3] == 'm' &&
	    s[4] == ' ') {
		*field = s + 5;
		return(FROM);
	}
	for (h = hb_tab; h->hb_t; h++) {
		p = s;
		q = h->hb_s;
		while (*q && *p) {
			if (*p == ' ')
				p++;
			else if (*p == *q || (*p >= 'A' && *p <= 'Z' &&
			    *p - 'A' + 'a' == *q)) {
				p++;
				q++;
			} else
				goto contin;
		}
		if (*q)
			continue;
		if (*p == ' ')
			p++;
		if (*p++ != ':')
			continue;
		if (*p == ' ')
			p++;
		*field = p;
		return(h->hb_t);
	    contin:;
	}
	*field = s;
	return(OTHER);
}

/*
 * Write header hp on stream fp in 'A' format
 */
h_awrite(hp, fp)
register struct hbuf *hp;
register FILE *fp;
{
	ngdel(strcpy(bfr, hp->h_ngs));
	fprintf(fp, "%c%s\n%s\n%s!%s\n%s\n%s\n",
		PROTO, hp->h_name, bfr, SYSNAME,
		hp->h_from, hp->h_adate, hp->h_title);
	h_optw(hp, fp);
	putc('\n', fp);
}

/*
 * Write header hp on stream fp in 'B' format.
 * Some 'internal' fields are only written if savei is true.
 */
h_bwrite(hp, fp, savei)
register struct hbuf *hp;
register FILE *fp;
bool savei;
{
	ngdel(strcpy(bfr, hp->h_ngs));
	fprintf(fp, "From: %s!%s\nNewsgroups: %s\n", SYSNAME, hp->h_from, bfr);
	fprintf(fp, "Title: %s\nArticle-I.D.: %s\n", hp->h_title, hp->h_name);
	fprintf(fp, "Posted: %s\n", hp->h_adate);
	h_optw(hp, fp);
	if (savei) {
		/* some fields for internal use only. */
		fprintf(fp, "Received: %s\n", hp->h_rdate);
		fprintf(fp, "Lines: %6d\n", hp->h_nlines);
	}
	putc('\n', fp);
}


/*
 * Write the optional header lines in hp on fp
 */
h_optw(hp, fp)
register struct hbuf *hp;
register FILE *fp;
{
	register struct hbuf_opts *h;

	if (hp->h_refs && *hp->h_refs)
		fprintf(fp, "References: %s\n", hp->h_refs);
	if (hp->h_reply && *hp->h_reply)
		fprintf(fp, "Reply-To: %s\n", hp->h_reply);
	if (hp->h_edate && *hp->h_edate)
		fprintf(fp, "Expires: %s\n", hp->h_edate);
	if (hp->h_control && *hp->h_control)
		fprintf(fp, "Control: %s\n", hp->h_control);
	for (h = hp->h_options; h != NULL; h = h->ho_l) {
		fputs(h->ho_p, fp);
		putc('\n', fp);
	}
}

/*
 * Initialize (to NULL) dynamic strings at hbuf hp
 */
h_init(hp)
register struct hbuf *hp;
{
	hp->h_from = NULL;
	hp->h_title = NULL;
	hp->h_ngs = NULL;
	hp->h_edate = NULL;
	hp->h_reply = NULL;
	hp->h_control = NULL;
	hp->h_refs = NULL;
	hp->h_path = NULL;
	hp->h_options = NULL;
}

/*
 * Free dynamic strings at hbuf hp
 */
h_free(hp)
register struct hbuf *hp;
{
	register struct hbuf_opts *h1, *h2;

	dfree(&hp->h_from);
	dfree(&hp->h_title);
	dfree(&hp->h_ngs);
	dfree(&hp->h_edate);
	*hp->h_rdate = '\0';
	dfree(&hp->h_reply);
	dfree(&hp->h_control);
	dfree(&hp->h_refs);
	dfree(&hp->h_path);
	h1 = hp->h_options;
	while (h1 != NULL) {
		h2 = h1->ho_l;
		free((char *)(h1->ho_p));
		free((char *)h1);
		h1 = h2;
	}
	hp->h_options = NULL;
}

/*
 * Print header hp on stream fp.
 * If lflag, then print short form of header.
 */
h_print(hp, lflag, fp)
register struct hbuf *hp;
register bool lflag;
register FILE *fp;
{
	if (!lflag)
		fprintf(fp, "From: %s %s\n", hp->h_from, hp->h_adate);
	sprintf(bfr, "(%s)", hp->h_name);
	fprintf(fp, lflag? "%-15s": "%s ", bfr);
	ngdel(strcpy(bfr, hp->h_ngs));
	fprintf(fp, lflag? "%-20s: %s\n": "%s : %s\n", bfr, hp->h_title);
	if (!lflag)
		h_optw(hp, fp);
	putc('\n', fp);
}

/*
 * Count the number of remaining lines in file fp.
 * Do not move the file pointer.
 */
linecnt(fp)
register FILE *fp;
{
	off_t curpos;
	register int nlines, c;

	if (fp == NULL)
		return(0);
	curpos = ftell(fp);
	nlines = 0;
	while ((c = getc(fp)) != EOF)
		if (c == '\n')
			nlines++;
	fseek(fp, curpos, 0);
	return(nlines);
}
