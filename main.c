/*
 *	NEWS program - version 1.13.
 *
 *	May 12, 1983
 *
 *	by Stephen Daniel (duke!swd)
 *	and Tom Truscott  (duke!trt).
 *
 */

#include "param.h"
#include <grp.h>

extern	struct	group *getgrnam();

/*
 * Main deciphers the argument list.
 */

/* local defines for main */

#define UNKNOWN 00001	/* possible modes for news program */
#define READ	00002
#define INPUT	00004
#define UIN	00010
#define SUBSCR	00020
#define BIT	00040
#define	RI	00100	/* read or input mode. kludgy kludgy. */
#define	ALIST	00200
#define	EXPUNGE	00400
#define	SUBLIST	01000

#ifndef SYSBUF
char SYSBUF[BUFSIZ];	/* to buffer std out */
#endif

struct {			/* options table. */
	char	optlet;		/* option character. */
	char	filchar;	/* if to pickup string, fill character. */
	bool	*flagptr;	/* TRUE if have seen this opt. */
	int	oldmode;	/* OR of legal input modes. */
	int	newmode;	/* output mode. */
	char	**bufpp;	/* string buffer */
} *optpt, options[] = {
	'\0', ' ', &o_alflag, ALIST, ALIST, &namebuf,
	'l', '\0', &o_lflag, ALIST|READ|RI, READ, (char **)NULL,
	'p', '\0', &o_pflag, ALIST|READ|RI, READ, (char **)NULL,
	'r', '\0', &o_rflag, ALIST|READ|RI, READ, (char **)NULL,
	't', ' ', &o_tflag, READ|RI, READ, &titlebuf,
	'i', ' ', &o_iflag, INPUT|RI, INPUT, &header.h_title,
	'a', ' ', &o_aflag, READ|RI, READ, &adatebuf,
	'b', ' ', &o_bflag, READ|RI, READ, &bdatebuf,
	'e', ' ', &o_eflag, INPUT|RI, INPUT, &header.h_edate,
	'n', NGDELIM, &o_nflag, READ|RI|INPUT, RI, &header.h_ngs,
	's', NGDELIM, &o_sflag, SUBSCR, SUBSCR, &header.h_ngs,
	'S', NGDELIM, &o_sflag, SUBLIST, SUBLIST, (char **)NULL,
	'c', ' ', &o_cflag, ALIST|READ|RI|INPUT, RI, &header.h_control,
	'E', ' ', &o_expflag, EXPUNGE, EXPUNGE, &header.h_edate,
	'f', ' ', &o_refflag, READ|RI|INPUT, RI, &header.h_refs,
/*	'x', ' ', &o_xflag, READ|RI, READ, &xbuf,*/
	'\0', '\0', 0, 0, 0, (char **)NULL,
};

main(argc, argv)
int argc;
register char **argv;
{
	int i;
	register int mode;	/* mode of news program */
	register char *ptr, **pptr;
	int filchar;		/* fill character */
#ifdef SUIDBUG
	struct passwd *pw;
	struct group *gp;
#endif
	static char *N_ENV[] = { SYSPATH, NULL };

	cmdname = sname(argv[0]);
	fromtty = isatty(fileno(stdin));
	totty = isatty(fileno(stdout));
	mode = UNKNOWN;
	if (cmdname[0] == 'r') {
		rnews++;
		mode = UIN;	/* will make all options illegal. */
		if (fromtty || totty)
			xerror("sorry");
	}
	pager = getenv("PAGER");	/* get from user's environment */
	editor = getenv("EDITOR");	/* get from user's environment */
	if (editor == NULL)
		editor = "/bin/e";
#ifdef	PAGER
	if (pager == NULL)
		pager = PAGER;
#endif

	/* --- BEGIN SECURITY SECTION --- */
	alarm(0);			/* paranoia strikes deep */
	savmask = umask(N_UMASK);	/* save old mask, set restricted one */
	savenv = environ;		/* save old environ */
	environ = N_ENV;		/* set restricted environment */

	sigtrap = FALSE;	/* true if a signal has been caught */
	setsig(SIGQUIT);
	setsig(SIGHUP);
	setsig(SIGINT);
	/* SIGPIPE is ignored, which is kludgy but allows paging
	 * to be disabled if the pager is not found.
	 * There has to be a better way to do this, however.
	 */
	signal(SIGPIPE, SIG_IGN);

	/* make sure there are enough open files for news program */
	for (i = 3; i < 8; i++)
		close(i);
	/* --- END SECURITY SECTION --- */

	/* set up defaults and initialize. */
	errno = 0;	/* reset error indicator */
	time(&now);

	if ((ptr = sitename()) == NULL)
		xerror("no site name!");
	strncpy(SYSNAME, ptr, SNLN);
	SYSNAME[SNLN] = '\0';

	/* allocate all stack space that will be needed */
	fixstack();
	/* Get all normally needed memory with a single sbrk */
	/* Allocations are tuned to avoid fragmentation */
	artsiz = (ARTSIZ <= 500? ARTSIZ: 50);
	free((char *)xmalloc((MTYPE)
		(artsiz*sizeof(struct artlist)+sizeof(struct urec)+2*BUFSIZ)));
	setbuf(stdout, SYSBUF);

	if ((unsigned)(uid = getuid()) >= (unsigned)USIZE)
		lxerror("login uid too big");
	gid = getgid();
#ifdef SUIDBUG
	if (uid == 0 && geteuid() == 0) {
		/*
		 * Must go through with this kludge since
		 * some systems do not honor the setuid bit
		 * when root invokes a setuid program.
		 */
		if ((pw = getpwnam(NEWSU)) == NULL)
			lxerror("Cannot get NEWSU pw entry");
		duid = pw->pw_uid;
		if ((gp = getgrnam(NEWSG)) == NULL)
			lxerror("Cannot get NEWSG gr entry");
		dgid = gp->gr_gid;
	}
#endif

	pptr = NULL;

	/* loop once per arg. */
	while (--argc) {
	    if (**++argv == '-') {
		/* kludge so "news -[ab] -2 days" works */
		if ((!argv[0][1] || isdigit(argv[0][1]))
		&&  (pptr == &adatebuf || pptr == &bdatebuf))
			goto isstring;

		/* Definitely options.  Locate and process them. */
		pptr = NULL;
		while (*++*argv != '\0') {
			for (optpt = options+1; optpt->optlet != '\0'; ++optpt) {
				if (optpt->optlet == **argv)
					goto found;
			}
			/* unknown option letter */
			sprintf(bfr, "Unknown option '%c'", **argv);
			xerror(bfr);

		    found:;
			if (*optpt->flagptr == TRUE || (mode != UNKNOWN &&
			    (mode&optpt->oldmode) == 0)) {
				sprintf(bfr, "Misused '%c' option", **argv);
				xerror(bfr);
			}
			if (mode == UNKNOWN || mode == RI)
				mode = optpt->newmode;
			*optpt->flagptr = TRUE;
			if (optpt->bufpp) {
				filchar = optpt->filchar;
				pptr = optpt->bufpp;
				dstrcpy(pptr, NULLSTR);
			}
		}

	    } else {
	isstring:;
		/*
		 * Pick up a piece of a string and put it into
		 * the appropriate buffer.
		 */
		/* if no option letter, assume a list of articles. */
		if (pptr == NULL && (mode == UNKNOWN || mode == READ)
		 && (!o_tflag && !o_nflag && !o_aflag && !o_bflag)) {
			optpt = options;
			mode = optpt->newmode;
			*optpt->flagptr = TRUE;
			filchar = optpt->filchar;
			pptr = optpt->bufpp;
			dstrcpy(pptr, NULLSTR);
		}
		if (pptr == NULL) {
			sprintf(bfr, "Bad option string \"%.40s\"", *argv);
			xerror(bfr);
		}
		dstrcat(pptr, *argv);
		if (**argv == '\0' || argv[0][strlen(*argv)-1] != filchar) {
			bfr[0] = filchar;
			bfr[1] = '\0';
			dstrcat(pptr, bfr);
		}
	    }
	}
	/* Strip trailing "," from References: field.
	 * This is an unfortunate kludge.
	 * A systematic "fill character" method is needed.
	 */
	if (header.h_refs)
		ngdel(header.h_refs);

	/*
	 * ALL of the command line has now been processed. (!)
	 */
	switch (mode) {

		case UNKNOWN:
		case RI:
		case READ:
		case ALIST:
			readr();
			break;

		case INPUT:
			iopt();
			break;

		case UIN:
			uneti();
			break;

		case EXPUNGE:
		case BIT:
			expire();
			break;

		case SUBSCR:
			subscr();
			break;

		case SUBLIST:
			sublist();
			break;

		default:
			lxerror("Bad mode");
			break;
	}

	dfree(&username);
	dfree(&userhome);
	if (bitup)
		makebits();
	xxit(0);
}

setsig(n)
{
	if (((int)signal(n, SIG_IGN)&01) == 0 && !rnews)
		signal(n, onsig);
}
