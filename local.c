#include "param.h"

/*
 * This file contains various routines
 * that have been found to vary on different UNIX systems.
 * You may need to tailor these or add others here.
 */

/*
 * Returns pointer to name of local system, null terminated.
 * Returns NULL if unable to determine local name.
 * Warning: returns pointer to static buffer.
 */
char *
sitename()
{

#ifdef	UNAME
	/* Use uname(III) library routine */
#include <sys/utsname.h>
	static struct utsname utsn;
	extern struct utsname *uname();

	if (uname(&utsn) == (struct utsname *)-1)
		return(NULL);
	return(utsn.nodename);
#endif

#ifdef	ETCUNAME
	Code not yet tested
	/* "/etc/whoami" contains system name terminated by newline */
	static char sitebfr[10];
	register FILE *fp;

	fp = xfopen("/etc/whoami", "r");
	sitebfr[0] = '\0';
	fgets(sitebfr, sizeof(sitebfr), fp);
	xfclose(fp);
	if (nstrip(sitebfr))
		xerror("bad /etc/whoami");
	return(sitebfr);
#endif

#ifdef	GETWHOAMI
	Code not yet tested
	/* Read <whoami.h> at run-time to get system name */
	static char sitebuf[100];
	register FILE *fp;
	register char *p, *t;

	fp = xfopen("/usr/include/whoami.h", "r");
	while (fgets(sitebuf, sizeof(sitebuf), fp) != NULL) {
		p = sitebuf;
		if (*p != '#')
			continue;
		p = nonspace(p+1);
		if (strncmp(p, "define", 6))
			continue;
		p = nonspace(p+6);
		if (strncmp(p, "sysname", 7))
			continue;
		p = nonspace(p+7);
		if (*p++ != '"')
			continue;
		for (t = p; *t != '"'; t++)
			if (*t == '\0')
				goto failret;
		*t = '\0';
		if (*p == '\0')
			goto failret;
		xfclose(fp);
		return(p);
	}
failret:
	xfclose(fp);
	return(NULL);
#endif

#ifdef	CCWHOAMI
	/* Compile <whoami.h> directly into news program */
#include <local/whoami.h>
	return(sysname);
#endif

}

/*
 * Return pointer to last component of a pathname.
 * Eg. given "/bin/sh", returns pointer to "sh".
 * Warning: no copy is made.
 */
char *
sname(s)
register char *s;
{
	register char *p;

	for (p = s; *p;)
		if (*p++ == '/')
			s = p;
	return(s);
}

/*
 * Extend stack so it will never grow again.
 * Otherwise an unrecoverable stack fault could occur.
 */
fixstack()
{
	char a[MAXLEN];

	a[0] = 0; a[MAXLEN-1] = 0;
}

/*
 * Return the ptr in sp at which the character c appears;
 * NULL if not found
 *
 * (Some Unix systems call this strchr, notably PWB 2.0
 * and its derivitives such as Unix/TS 2.0, Unix 3.0, etc.)
 */

char *
index(sp, c)
register char *sp, c;
{
	do {
		if (*sp == c)
			return(sp);
	} while (*sp++);
	return(NULL);
}
