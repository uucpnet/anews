#include "param.h"
#include <sys/stat.h>
#include <sys/dir.h>

#define	SLPTIME	15

/*
 * Delete articles which have expired.
 * Also delete article "stubs"
 * (zero-length files used to prevent duplicate receipt).
 * Reconnect articles not in nindex
 */

static char DROPPING[] = "dropping %s\n";

expire()
{
	struct nrec nrec;
	struct direct *dentp;
	struct stat st;
	struct hbuf h;
	register FILE *fp;
	DIR *dirp;
	register int i;
	register struct artlist *abase, *afree;
	time_t exptime, t, lastdate;
	bool dosort, reattach;

	/* always update bitmap, so expire() always resynchs it */
	bitup = TRUE;

	dosort = FALSE;

	/* if no expunge date specified, use default */
	if (header.h_edate[0] == '\0')
		dstrcpy(&header.h_edate, uudead);
	if ((exptime = atot(header.h_edate)) < 0 || exptime >= now)
		xerror("Bad expunge date");

	printf("Looking for expired articles\n");
	fflush(stdout);
	h_init(&h);
	abase = (struct artlist *)xmalloc(ARTSIZ*sizeof(struct artlist));
	afree = abase;
	lastdate = 0;
	n_openr();
	while (n_read(&nrec)) {
		if (sigtrap)
			goodbye();
		if (nrec.n_date < lastdate) {
			if (!dosort) {
				log("nindex order error");
				dosort = TRUE;
			}
		}
		lastdate = nrec.n_date;
		if ((fp = ah_read(&h, nrec.n_name)) == NULL) {
			/* drop garbled article */
			if (afree >= &abase[ARTSIZ])
				break;
			strcpy(afree->a_name, nrec.n_name);
			afree->a_date = nrec.n_date;
			afree->a_status = 0;
			afree++;
			continue;
		}
		xfclose(fp);
		if (h.h_edate && (t = atot(h.h_edate)) > 0
		&&  t < now) {
			/* drop expired article */
			mkaname(lbfr, nrec.n_name);
			canned(lbfr, nrec.n_name, FALSE);
			if (afree >= &abase[ARTSIZ])
				break;
			strcpy(afree->a_name, nrec.n_name);
			afree->a_date = nrec.n_date;
			afree->a_status = 0;
			afree++;
			continue;
		}
	}
	n_close();
	h_free(&h);

	/* If any articles were found, clean them up */
	ninclean(abase, afree);
	free((char *)abase);

/*
 * Check directories for dead articles
 */

	printf("Looking for dead stubs, unattached articles\n");
	fflush(stdout);
	reattach = FALSE;
#ifdef	SUBDIR
	for (i = 0; i < SUBDIR; i++) {
#if	(SUBDIR <= 10)
		sprintf(lbfr, "%s/%d", ARTD, i);
#else
		sprintf(lbfr, "%s/%02d", ARTD, i);
#endif
#else
	{
		strcpy(lbfr, NEWSD);
#endif
		chdir(lbfr);
		dirp = opendir(".", "r");
		if (dirp == NULL) {
			sprintf(bfr, "Cannot open %s\n", lbfr);
			lxerror(bfr);
		}
		while ((dentp = readdir(dirp)) != NULL) {
			if (sigtrap)
				goodbye();
			strcpy(lbfr, dentp->d_name);
			/* ignore '.' and '..' */
			if (lbfr[0] == '.' &&
			(lbfr[1]=='\0' || (lbfr[1]=='.' && lbfr[2]=='\0')))
				continue;
			if (!isarticle(lbfr) || stat(lbfr, &st)) {
#ifdef SUBDIR
				printf("Bad article %s\n", lbfr);
#endif
				continue;
			}
			if (st.st_atime < now
			 && (fp = ah_read(&h, lbfr)) != NULL) {
				xfclose(fp);
				/* reattach article */
				strcpy(nrec.n_name, h.h_name);
				strcpy(nrec.n_ings, h.h_ngs);
#ifdef	GETDATE
				nrec.n_date = atot(h.h_rdate);
#else
				nrec.n_date = st.st_ctime;
#endif
				if (!reattach) {
					reattach = TRUE;
					dosort = TRUE;
					lock();
					log("re-attaching article(s)");
					n_opena();
				}
				n_write(&nrec);
			}
			if (st.st_ctime < exptime && st.st_size == 0) {
				/* drop stub */
				unlink(lbfr);
				printf(DROPPING, lbfr);
			}
		}
		closedir(dirp);
	}
	if (reattach) {
		n_close();
		unlock();
		sleep(SLPTIME);
	}
	if (dosort) {
		lock();
		log("resorting nindex");
		chdir(NEWSD);
		fwsubr(xshell, SORTNINDEX, (char *)NULL);
		unlock();
	}
	printf("Done\n");
}

/*
 * delete nindex entries of cancelled and missing articles
 */
ninclean(basep, freep)
register struct artlist *basep, *freep;
{
	register struct artlist *aptr, *taptr;
	struct nrec nrec;
	time_t starttime, stoptime;

	/* compress artlist to just the banished articles */
	taptr = basep;
	for (aptr = taptr; aptr < freep; aptr++)
		if ((aptr->a_status&A_EXIST) == 0)
			*taptr++ = *aptr;
	freep = taptr;
	if (freep == basep)
		return;

	/* Trick to "speed up" cancel requests */
	if (freep - basep < 5)
		if (dofork())
			return;

	/*
	 * Sort up by date to avoid quadratic search.
	 * *** This assumes nindex is sorted up. ***
	 */
	if (o_rflag)
		revart(basep, freep);

	/* sweep nindex and delete the cancelled entries */
	aptr = basep;
	lock();
	time(&starttime);
	n_openm();
	while (n_read(&nrec)) {
		if (aptr >= freep || nrec.n_date < aptr->a_date) {
			n_write(&nrec);
			continue;
		}
		if (strcmp(nrec.n_name, aptr->a_name) != 0) {
			/* nindex order error */
			n_write(&nrec);
			continue;
		}
/*DBG		printf(DROPPING, nrec.n_name);*/
		bitup = TRUE;
		aptr++;
	}
	n_close();
	time(&stoptime);
	if (stoptime - starttime > 30) {
		unlock();
		sleep(SLPTIME);	/* Give others a chance */
		lock();
	}
	if (bitup)
		makebits();
	unlock();
}
