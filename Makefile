NEWSDIR = /usr/spool/news
ARTDIR	= /usr/spool/articles
RECDIR = /usr/lib
BIN = /usr/bin
NEWS = news

NEWSUSR = daemon
NEWSGRP = daemon

# following definitions pass the above variables to extern.c
EXTERNF1 = '-DNEWSDIR="$(NEWSDIR)' '-DARTDIR="$(ARTDIR)'
EXTERNF2 = '-DNEWSUSR="$(NEWSUSR)"' '-DNEWSGRP="$(NEWSGRP)"'

CC = cc
CFLAGS = -O
LFLAGS = -i -s -llocal
OFILES = cancel.o control.o expire.o extern.o fio.o header.o input.o\
	  local.o main.o makebits.o misc.o readr.o subscr.o
LFILES = cancel.c control.c expire.c extern.c fio.c header.c input.c\
		local.c main.c makebits.c misc.c readr.c subscr.c
TFILES = NOTICE README.ms DASHC CHANGES TODO sublist\
	Makefile local.h.safe local.h param.h\
	$(LFILES) news.install news.1 newscheck.c newscheck.1 uurec.c\
	dirconvert news.daily ngmap.c recnews.c sublist newsrec.c newsrecover.sh
PFILES = local.h param.h

all:	$(NEWS) 

$(NEWS):	$(OFILES)
	$(CC) $(OFILES) $(LFLAGS) -o $(NEWS)

newsrec: newsrec.o
	$(CC) newsrec.o -o newsrec

cp:	$(NEWS) newsrecover.sh newsrec
	cp $(NEWS) $(BIN)/$(NEWS)
	cp newsrec $(RECDIR)
	chmod 755 $(RECDIR)
	cp newsrecover.sh  $(BIN)
	chown $(NEWSUSR) $(BIN)/$(NEWS)
	chgrp $(NEWSGRP) $(BIN)/$(NEWS)
	chmod 6755 $(BIN)/$(NEWS)
	rm -f $(BIN)/rnews
	ln $(BIN)/$(NEWS) $(BIN)/rnews


install:	cp
	sh news.install $(NEWSDIR) $(ARTDIR) $(NEWSUSR) $(NEWSGRP)

lint:	$(LFILES)
	lint -bah $(LFILES)
	-touch lint

clean:
	rm -f $(NEWS) $(OFILES) lint uurec newsrec newsrec.o ngmap newscheck news.tar

tar:	news.tar

news.tar: $(TFILES)
	tar cf news.tar $(TFILES)

$(OFILES):	$(PFILES)

.c.o:;	$(CC) $(CFLAGS) -c $*.c

extern.o:	extern.c $(PFILES) Makefile
	$(CC) $(CFLAGS) $(EXTERNF1) $(EXTERNF2) -c extern.c

newscheck:	newscheck.c
	$(CC) $(CFLAGS) -s $(EXTERNF1) newscheck.c -o newscheck

uurec:	uurec.c
	$(CC) $(CFLAGS) -n -s uurec.c -o uurec

ngmap:	ngmap.c
	$(CC) $(CFLAGS) -n -s ngmap.c -o ngmap
