#include "param.h"

#ifndef	NEWSDIR
/* NOTE: following line does not have a trailing quote. */
#define	NEWSDIR	"/usr/spool/news
#endif

#ifndef	ARTDIR
#define	ARTDIR	"/usr/spool/articles
#endif
#ifndef	NEWSUSR
#define	NEWSUSR	"daemon"
#endif

#ifndef	NEWSGRP
#define	NEWSGRP	"daemon"
#endif

char	*cmdname;	/* Name of news program, used for diagnostics */
id_t	uid, gid;	/* real user/group id */
int	savmask;	/* old umask. */
char	**savenv;	/* old environ pointer */
bool	sigtrap;	/* true if a signal has been caught */
bool	bitup;		/* true if must re-create bitmap. */
bool	rnews;		/* true if process name is rnews */
bool	fromtty;	/* true if isatty(fileno(stdin)) */
bool	totty;		/* true if isatty(fileno(stdout)) */
bool	o_lflag, o_pflag, o_rflag, o_tflag, o_iflag,
	o_aflag, o_bflag, o_eflag, o_nflag, o_sflag, o_expflag,
	o_cflag, o_alflag, o_refflag, o_xflag;

time_t	now;
struct	hbuf	header;
char	bfr[MAXLEN];	/* scratch area. use with caution! */
char	lbfr[2*BUFLEN];	/* small scratch area. avoid using. */
char	NULLSTR[1];		/* Handy null string */
char	*username;		/* user login name */
char	*userhome;		/* user home directory */
unsigned artsiz;		/* max. articles readable by readr() */
char	*pager;			/* pager program (from user's environment) */
char	*editor;		/* text editor (from user's environment) */
char	*adatebuf;		/* buffer for 'a' option */
char	*bdatebuf;		/* buffer for 'b' option */
char	*titlebuf;		/* title buffer */
char	*namebuf;		/* for list of article names (duke.101) */
char	*xbuf;			/* command string */

char	NEWSD[] = NEWSDIR";		/* news utility files directory. */
char	ARTD[] = ARTDIR";		/* articles directory. */
char	CAND[] = NEWSDIR/canned";	/* Directory for cancelled news */

char	LOCKFILE[] = NEWSDIR/LOCK";	/* lock file. (linked to uindex). */
char	BITFILE[] = NEWSDIR/bitfile";	/* bit map file. */
char	SEQFILE[] = NEWSDIR/seq";	/* current sequence number. */
char	NGFILE[] = NEWSDIR/ngfile";	/* list of legal newsgroups. */
char	UINDEX[] = NEWSDIR/uindex";	/* user index file. */
char	NINDEX[] = NEWSDIR/nindex";	/* news index file. */
char	SYSFILE[] = NEWSDIR/sys";	/* system subscriptions. */
#ifdef LOG
char	LOGFILE[] = NEWSDIR/log";	/* log of interesting things */
#endif
char	SUBLIST[] = NEWSDIR/sublist";	/* Annoted newsgroup listing */
char	TUFILE[] = NEWSDIR/TMP1";	/* temp files. */
char	TNFILE[] = NEWSDIR/TMP2";

char	SYSNAME[SNLN+1];		/* local system name */
#ifdef SUIDBUG
id_t	duid, dgid;	/* user/group that news would like to be. */
char	NEWSU[] = NEWSUSR;
char	NEWSG[] = NEWSGRP;
#endif

char	SNGDFLT[] = "general";		/* default subscription list */
char	INGDFLT[] = "general";		/* default insertion newsgroup list */
char	SNGMUST[] = "general";		/* required subscription list */
char	uudead[] = "-10 days";		/* Max skew in article reception */
char	uucancel[] = "-2 days";	/* Max art age permitting network cancel */

char	PARTIAL[] = "dead.article";	/* place to save partial news. */
char	NETCHRS[] = "!:.@^";		/* punctuation used for various nets */
