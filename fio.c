#include "param.h"

/*
 * This file contains a set of routines
 * for accessing various files.
 */

static	FILE	*sysfile;
static	FILE	*uindex, *nindex;
static	FILE	*tuout, *tnout;
static	int	nmode, umode;
static	off_t	uinoff;		/* last offset in .nindex file */
static	int	uinlen;		/* length of last record read */
static	char	*MAILFILE;	/* file name of temp file used to send mail. */
static	char	*mailaddr;	/* saved addr of where to send mail. */
#ifdef SMAILER
static	char	*smaill;	/* subject line for s-mailers */
#endif

static char *
fldget(q, p)
register char *q, *p;
{
	while (*p && *p != ':') {
		if (*p == '\\' && p[1]==':')
			p++;
		*q++ = *p++;
	}
	*q = '\0';
	return(p);
}


#define	READ	0
#define	MODIFY	1
#define	APPEND	2

/*
 * Open uindex for reading only.
 */
u_openr()
{
	uinoff = 0;
	uinlen = 0;
	uindex = xfopen(UINDEX, "r");
	umode = READ;
}

/*
 * Open uindex for modifying.
 */
u_openm()
{
	u_openr();
	tuout = xfopen(TUFILE, "w");
	umode = MODIFY;
}

/*
 * Rewind uindex file for re-reading.
 * (Fast equivalent of u_close(); u_openr())
 */
u_rewind()
{
	if (uinoff+uinlen) {
		fseek(uindex, (off_t)0, 0);
		uinoff = 0;
		uinlen = 0;
	}
}

/*
 * Open nindex for reading only.
 */
n_openr()
{
	nindex = xfopen(NINDEX, "r");
	nmode = READ;
}

/*
 * Open nindex for reading,
 * positioned at first entry with date >= t.
 */
n_openl(t)
time_t t;
{
	register FILE *fp;
	register char *p;
	off_t low, mid, high;

	n_openr();
	if (t == 0)
		return;
	fp = nindex;		/* shorthand */
	low = 0;		/* wanted entry begins no earlier than this */
	high = filelen(NINDEX);	/* wanted entry begins no higher than this */
	/* Try to pick accurate, but low, initial guess */
	if (high > 2*BUFSIZ)
		mid = high - 2*BUFSIZ;
	else
		mid = 0;

	/* binary search */
	while (high - low > 3*BUFSIZ) {
		mid = mid - mid%BUFSIZ;	/* round down to BUFSIZ boundary */
		fseek(fp, mid, 0);
		if (fgets(bfr, BUFLEN, fp) == NULL
		||  fgets(bfr, BUFLEN, fp) == NULL)
			goto ishigh;
		p = bfr; while (*p++ != ':');
		if (atol(p) >= t)
		ishigh:
			high = mid;
		else
			low = mid;
		mid = (high + low)/2;
	}

	/* linear search */
	fseek(fp, low, 0);
	if (low != 0)
		fgets(bfr, BUFLEN, fp);
	while (fgets(bfr, BUFLEN, fp) != NULL) {
		p = bfr; while(*p++ != ':');
		if (atol(p) >= t) {
			fseek(fp, -(off_t)strlen(bfr), 1);
			break;
		}
	}
}

/*
 * Open nindex for modifying.
 */
n_openm()
{
	n_openr();
	tnout = xfopen(TNFILE, "w");
	nmode = MODIFY;
}

/*
 * Open nindex for appending.
 */
n_opena()
{
	nindex = xfopen(NINDEX, "a");
	tnout = nindex;
	nmode = APPEND;
}

/*
 * Read a record from uindex.
 */
u_read(up)
struct urec *up;
{
	register char *p;

	uinoff += uinlen;
	p = bfr;
	if (fgets(p, MAXLEN, uindex) == NULL)
		return(FALSE);
	uinlen = strlen(p);
	if (!nstrip(p))
		lxerror("uindex record too long");
	up->u_uid = atoi(p);
	if ((unsigned)up->u_uid >= (unsigned)USIZE)
		lxerror("uindex uid out of range");
	while (*p && *p != ':')
		p++;
	if (*p++ == '\0')
		goto bad;
	up->u_date = atol(p);
	while (*p && *p != ':')
		p++;
	strcpy(&up->u_sngs[0], SNGDFLT);
	if (*p++)
		strcpy(&up->u_sngs[0], p);
	ngcat(&up->u_sngs[0]);
	return(TRUE);
bad:
	lxerror("Bad uindex format");
	/* NOTREACHED */
}

/*
 * Read a nindex record.
 */
n_read(np)
register struct nrec *np;
{
	register char *p;

	p = bfr;
	if (fgets(p, BUFLEN, nindex) == NULL)
		return(FALSE);
	if (!nstrip(p))
		lxerror("nindex record too long");
	p = fldget(&np->n_name[0], p);
	if (*p++ == '\0')
		goto bad;
	np->n_date = atol(p);
	while (*p && *p != ':')
		p++;
	strcpy(&np->n_ings[0], INGDFLT);
	if (*p++)
		strcpy(&np->n_ings[0], p);
	ngcat(&np->n_ings[0]);
	return(TRUE);
bad:
	lxerror("Bad nindex format");
	/* NOTREACHED */
}

/*
 * Write a uindex record.
 */
u_write(up)
struct urec *up;
{
	ngdel(strcpy(bfr, up->u_sngs));
	if (strcmp(bfr, SNGDFLT) == 0)
		fprintf(tuout, "%d:%ld\n", up->u_uid, (long)up->u_date);
	else
		fprintf(tuout, "%d:%ld:%s\n", up->u_uid, (long)up->u_date, bfr);
}

/*
 * Write a nindex record.
 */
n_write(np)
register struct nrec *np;
{
	ngdel(strcpy(bfr, np->n_ings));
	if (strcmp(bfr, INGDFLT) == 0)
		fprintf(tnout, "%s:%ld\n", np->n_name, (long)np->n_date);
	else
		fprintf(tnout, "%s:%ld:%s\n", np->n_name, (long)np->n_date, bfr);
}

/*
 * Copy the balance of the .nindex file.
n_copy()
{
	register int c;

	while ((c = getc(nindex)) != EOF)
		putc(c, tnout);
}
 */

/*
 * Close uindex.
 */
u_close()
{
	xfclose(uindex);
	if (umode == MODIFY) {
		xfclose(tuout);
		rename(TUFILE, UINDEX);
	}
	uindex = tuout = NULL;	/* Error guard */
}

/*
 * Close nindex.
 */
n_close()
{
	xfclose(nindex);
	if (nmode == MODIFY) {
		xfclose(tnout);
		rename(TNFILE, NINDEX);
	}
	nindex = tnout = NULL;	/* Error guard */
}

/*
 * Rename f1 to f2
 * A sync() occurs at the end to minimize the danger of a crash
 * causing file information to be left incore.
 * It would also help to {sync(); sleep(2);} at the beginning.
 * Bug: if the final unlink fails then uindex (or nindex) will be linked
 * to its temp file, causing uindex to get clobbered later
 * when the temp file is creat(II)ed.
 * This could be checked for in u_openm() and n_openm().
 */
static
rename(f1, f2)
register char *f1, *f2;
{
	if (filelen(f1) == -1) {
		sprintf(bfr, "Rename: no such file %s", f2);
		lxerror(bfr);
	}
	unlink(f2);
	if (link(f1, f2)) {
		sprintf(bfr, "Rename: cannot link %s to %s", f1, f2);
		lxerror(bfr);
	}
	unlink(f1);
	sync();	/* Defend against crash on system with incore buffer list */
}

/*
 * Update uindex entry, keyed by uid.
 * Has fast check to speed things up.
 * If read/write io is permitted, it is even faster.
 */
u_update(t)
time_t t;
{
	register FILE *fp;
	register char *p;
	register struct urec *up;

#ifdef	USE_RDWR
	fp = xfopen(UINDEX, "r+");
#else
	fp = xfopen(UINDEX, "r");
#endif
	/* Check for beginning of line */
	if (uinoff) {
		fseek(fp, uinoff-1, 0);
		if (getc(fp) != '\n')
			goto slowcode;
	}
	/* Check uid */
	if ((p = fgets(bfr, BUFLEN, fp)) == NULL || atoi(p) != uid)
		goto slowcode;
	if ((p = index(p, ':')) == NULL)
		goto slowcode;
	/* Check date */
	if (atol(++p) > t) {
		xfclose(fp);
		return;
	}
	if ((p = index(p, ':')) == NULL)
		goto slowcode;
	sprintf(lbfr, "%d:%ld", uid, t);
	/* Check old and new line lengths */
	if (p-&bfr[0] != strlen(lbfr))
		goto slowcode;
#ifndef	RDWR
	xfclose(fp);
	fp = xfopen(UINDEX, "a");
#endif
	fseek(fp, uinoff, 0);
	fputs(lbfr, fp);
	xfclose(fp);
	return;

slowcode:
	xfclose(fp);
	up = (struct urec *)xmalloc(sizeof(struct urec));
	u_openm();
	while (u_read(up)) {
		if (uid == up->u_uid && up->u_date < t)
			up->u_date = t;
		u_write(up);
	}
	u_close();
	free((char *)up);
}

/*
 * Open SYSFILE.
 */
s_openr()
{
	sysfile = xfopen(SYSFILE, "r");
}

/*
 * Read SYSFILE.
 */
char s_flags[] = "ABN";
s_read(sp)
register struct srec *sp;
{
	register char *p;
	register char *q;

	p = bfr;
	if (fgets(p, MAXLEN, sysfile) == NULL)
		return(FALSE);
	if (!nstrip(p))
		lxerror("SYSFILE line too long");
	sp->s_xmit[0] = '\0';
	sp->s_flags = 0;

	p = fldget(sp->s_name, p);
	if (*p++ == '\0')
		lxerror("Bad SYSFILE line");

	p = fldget(sp->s_sngs, p);
	ngcat(sp->s_sngs);
	if (*p++ == '\0')
		return(TRUE);

	p = fldget(bfr, p);
	for (q = bfr; *q && *q != ','; q++);
	*q = '\0';
	for (q = s_flags; *q; q++)
		if (index(bfr, *q))
			sp->s_flags |= (1<<(q-s_flags));
	if (*p++ == '\0')
		return(TRUE);

	fldget(sp->s_xmit, p);
	return(TRUE);
}

/*
 * Find the SYSFILE record for a system.
 */
s_find(sp, system)
register struct srec *sp;
char *system;
{
	s_openr();
	while (s_read(sp))
		if (strncmp(system, sp->s_name, SNLN) == 0) {
			s_close();
			return(TRUE);
		}
	s_close();
	return(FALSE);
}

/*
 * Close sysfile.
 */
s_close()
{
	xfclose(sysfile);
	sysfile = NULL;	/* Error guard */
}

/*
 * Routines for sending mail.
 *
 * Create a temp file for holding outbound mail.
 *
 * Systems with sophisticated mailers should use
 * the "repto" field (if it is not NULL) rather than addr,
 * which is just the return uucp path.
 */
FILE *
/*ARGSUSED*/
mail_open(addr, repto, subject)
register char *addr, *subject;
char *repto;
{
	register FILE *mf;

	MAILFILE = (char *)xmalloc(BUFLEN);
	mf = xfopen(nmktemp(MAILFILE, "mail"), "w");

#ifdef SMAILER
	smaill = NULL;
	if (subject && *subject)
		dstrcpy(&smaill, subject);
#else
	if (subject && *subject) {
		fprintf(mf, "Subject: %s\n\n", subject);
		fflush(mf);
	}
#endif
	mailaddr = NULL;
	dstrcpy(&mailaddr, addr);
	fixpath(mailaddr);	/* truncate comments on return address */
	return(mf);
}

/*
 * Send the accumulated letter.
 */
mail_close(mf)
register FILE *mf;
{
	xfclose(mf);

#ifdef SMAILER
	sprintf(bfr, "%s -s %s %s", SMAILER, smaill, mailaddr);
	dfree(&smaill);
#else
	sprintf(bfr, "%s %s", NMAILER, mailaddr);
#endif
	fwsubr(nshell, bfr, MAILFILE);
	dfree(&mailaddr);
	unlink(MAILFILE);
}

log(s)
register char *s;
{
	fflush(stdout);
	fprintf(stderr, "%s: %s.\n", cmdname, s);
#ifdef LOG
	{
	register FILE *lf;
	register char *timef;

	timef = ctime(&now);
	timef[16] = '\0';
	lock();
	lf = xfopen(LOGFILE, "a");
	fprintf(lf, "%s - %s.\n", timef+4, s);
	xfclose(lf);
	unlock();
	}
#endif
}
