.NH
News installation
.PP
This version of news is "A+" news and is not the
same as "B news" or "notesfile".
The different versions can communicate with each other
but are internally incompatible.
If you keep this in mind you will not be confused
by articles about features or bugs in someone else's "news" program.
.PP
The
.I getdate(3)
routine should be installed prior to
installation of
.I news.
It is available as ~uucp/getdate.tar on some sites.
.PP
Edit /usr/include/whoami.h and change the #define for sysname
to the name chosen for your system.
If your system does not have that file you will have to change
the "news.install" to correctly determine SYSNAME.
The way in which the news program itself determines
the local system name is described in local.h.
.PP
If you will be receiving news from other systems,
add an entry for "rnews" to the Cmds array of uuxqt.c,
then remake uux.
(Other useful uucp suggestions may be found in ~uucp/usenet.tar)
.PP
News runs setuid to keep its own files secure.
You must decide on the uid and gid to be used by
.I news.
In the supplied Makefile, these are both "daemon".
You should edit /etc/passwd and /etc/group
to include these two groups,
or you can change the news uid and/or gid
by changing the appropriate make variable (in Makefile).
.PP
Edit the file "local.h" which defines certain values
that might be changed locally.
The file "local.h.safe" is a very conservative file, for comparison.
.I News
may now be installed:
.DS
	su root
	make install
	make clean
.DE
Once the news program is fully operational
announce your site to the network by filling out "newsite.news"
and submitting it to the newsgroup "net.news.newsite".
.PP
News starts up much faster if it is made "sticky"
(as is the case with any large shared-text program).
The news.1 manual page may be put in /usr/man/man1/news.1.
Also, you will probably want to edit the "ngfile" and "sys" files
that are described below.
.PP
If you have an older version of news, the above "make install" sequence
may work correctly, but shell scripts and other news-related program
may stop working.  Problem areas are: making sure you do or do not have
subdirectories, getting file names right ('dot' files are no longer),
and cron-run programs which clean up after news.
Look at the CHANGES file for information about changes to news.
.NH
News Files
.NH 2
/usr/bin/news
.PP
This is the executable news program.
The program "rnews" is a link to
.I news
which,
when invoked,
reads a network-formatted article from its standard input.
.NH 2
/usr/spool/news
.PP
This directory contains the
files used by the news program.
It, and its subdirectories,
should be examined periodically for unusual or incorrect files.
.NH 2
/usr/spool/news/bitfile
.PP
This file is a bit map
maintained for the benefit of
.I login(1).
The following routine can be embedded
in login to check if a user has news:
.DS
#define BITFILE "/usr/spool/news/bitfile"
/* return 1 if user has news, else 0. */
newscheck(uid)
register int uid;
{
	register int fd;
	static char c;
	fd = open(BITFILE, 0);
	lseek(fd, (long)(uid>>3), 0);
	read(fd, &c, 1);
	close(fd);
	return((c >> (uid&07)) & 01);
}
.DE
.PP
The bit map is recreated when an article is cancelled,
and it is updated when each user reads his new news,
or an article is inserted.
Each user's bit is one if he has news, and zero if not.
.PP
The bit map may also be interrogated using the supplied
.I nchk
program.
.NH 2
/usr/spool/news/ngfile
.PP
This file lists all of the accepted news groups on the system.
Each newsgroup
to which an article is submitted
or to which a user subscribes
must appear in ngfile.
Newsgroups are listed in the ngfile one per line.
The news program adds entries to ngfile as needed,
but (currently) never deletes entries.
The local administration should prune ngfile periodically.
Never let it grow larger than 1500 characters.
A typical ngfile for system "xyz" would be:
.DS
general
sys
dept
net.general
test
to.duke
.DE
The "general" newsgroup should be used for most articles.
The "sys" newsgroup might be for news of interest
to systems programmers,
while the
"dept"
newsgroup would be for news of departmental
interest only
(e.g. parties).
Articles submitted to "net.general" are put on the net (see below).
The newsgroup "test" may be used for local tests of news.
The newsgroup "to.duke" sends news only to Duke
and may be used for network news tests.
Entries are made in this file by
.I news
when a local submission is made to a new newsgroup.
.NH 2
/usr/spool/news/sys
.PP
This file lists the newsgroups which
the local system and connecting remote systems
receive from the net.
The file for system xyz might be
.DS
xyz:net.all,to.xyz
duke:net.all,to.duke
.DE
Each entry consists of the system name, a colon,
and a list of comma-separated newsgroups.
(The line may also contain
two additional fields
which are described below under
"Transmission of network news.")
The systems xyz and duke subscribe to all "net" articles,
xyz alone subscribes to "to.xyz" (a test newsgroup),
and duke alone subscribes to "to.duke" (another test newsgroup).
The local administration may edit the sys file as desired.
For example, most of the systems
in the Research Triangle, NC,
subscribe to the sub-network newsgroup "triangle".
.NH 2
/usr/spool/news/sys.nnn
.PP
Files with names of this form (e.g. "duke.122") are news articles.
According to the protocol,
every system guarantees to give each article
it generates a unique file name.
This is done by appending
a unique integer
to the local system name.
The current version of
.I news
can be configured to hash these files
into subdirectories named "0" through "9".
The file would be put in the directory whose
name is the same as the last digit of the file name.
This makes the news spool directory much smaller,
which makes news much faster.
.NH 2
/usr/spool/news/xmitnnn
.PP
These temporary files are used to hold incoming and outgoing articles.
If subdirectories are used they will be put in the "etc" subdirectory.
.NH 2
/usr/spool/news/uindex
.PP
This file contains one line per news user.
Each line has three colon separated fields.
The first field is the user id.
The second field is the time (in seconds since 1970)
when the user last read news.
The third field is the subscription list,
which consists of newsgroups
separated by commas.
A missing third field is taken to be ":general".
.NH 2
/usr/spool/news/nindex
.PP
This file contains one line per news article.
The three colon separated fields on each line
are for the file name,
the date of submission (locally),
and the list of comma-separated newsgroups
to which the article belongs.
As with uindex,
a missing third field is taken to be ":general".
.NH 2
/usr/spool/news/seq
.PP
This file contains the sequence number given to
the last article generated locally.
.NH 2
/usr/spool/news/log
.PP
This file contains important diagnostics produced by news.
For example, the "duplicate article" diagnostic
indicates that articles are being received from more than one site
which may be a wasteful expense.
This file should be examined periodically
to see if something is amiss with news.
.NH
Network news
.NH 2
News article transfer format
.PP
.I News
supports two article transfer formats.
The so-called
.B B
format is preferred,
as it is the network standard format.
The
.B A
format is obsolete, and has been retained for
backwards compatibility with old news
systems and old news data bases.
.NH 3
The 'A' format
.PP
In this format,
the first character of the file is 'A'
(the format identifier).
The rest of the first line is a unique network-wide name "sys.nnn",
no longer than 14 characters,
where "sys" is the originating system name
and "nnn" is a unique integer on that system.
The article name is used to prevent
the unlimited duplication of news articles
that might otherwise occur.
The second line is a comma-separated list
of newsgroups to which the article belongs.
Newsgroup names may not contain colons.
The third line identifies the contributor of the article.
It is a system-pathname sequence
suitable for mailing a reply via
.I mail(1).
This line is also used as a list of all systems
which have seen the article.
News will not transmit this article to any system
whos name appears on this line.
The fourth line is the contribution date in
.I ctime(3)
format.
The fifth line is the article title.
The remaining lines are the text of the article.
The article is ending by '.' alone on a line, or end of file.
.DS
   Figure 1 - news transmission format.

Aduke.405		. Format id ('A') followed by
			  the article name.
test			. newsgroups list.
duke!swd		. path name of contributor.
Fri May 16 10:29:40 1980. date of original submission.
test			. article title.
testing one two three	. text of article.
.DE
.NH 3
The 'B' format for network news
.PP
This format was designed at Berkeley,
and is designed for compatibility with
the Arpanet standard format for mail,
RFC 733.
The header is a series of
lines of the form
.DS
Field-name: some interesting string
.DE
The field name must begin with a capital letter,
contain no white space and end with a colon.
After the colon there must be one or more spaces,
which are ignored.
The format of what comes after these spaces depends
on the field name.
The following is a list of supported field names,
and what field they correspond to in the 'A'
format.
.DS
From:		the contributer path line
To:		the newsgroup list
Newsgroups:	same as "To:"
Subject:	the article title
Title:		same as "Subject:"
Article-I.D.:	the article name
Posted:		the submission date
Received:	the date the article was received locally
Expires:	the date the article is to expire
Reply-To:	the author's return address
References:	names of articles to which this one refers
Control:	special article.
.DE
(The B format is in constant flux,
so the above information is surely inaccurate.)
In the current version of news "Reply-To:"
is not supported.
.PP
Any header line which is syntactically
correct but
does not have one of the above field names
is treated as a comment.
It is retained and printed with the news,
but is not otherwise processed.
The end of the header is signaled by
the first line which
does not fit the rules for valid header lines.
This line (which should be a blank line)
is thrown away, and the article
body follows it.
.NH 2
Transmission of network news.
.PP
When a news item is entered into
the local news system,
the sys file is scanned.
For each system in the file
whose name does not appear in the
contributor-pathname line of the item,
and whose subscription list
matches the item's newsgroup list,
.I news
attempts to
run
"rnews"
on the remote system with
the article as standard input.
If the last (fourth) field in the "sys"
file is empty,
then
.I news
uses
uux
to execute rnews on the remote system.
Otherwise, the fourth field in the
."sys"
file is a command line which
which is invoked with the article on standard input.
This transmission program (command line)
assumes responsibility for passing
the article unchanged
to
"rnews"
on the remote system.
Frequently, the program invoked will
be a network mail program.
Setting up
.I news
to use
.I mail(1)
for
transmission of articles is
described in detail in the
"Examples"
section below.
.PP
The third field of the "sys"
file
is used to pick the proper transmission format.
If this field is empty or contains a 'A',
then the
.B A
format will be used.
If it contains a 'B',
then the
.B B
format will be used.
.NH 2
Reception of network news.
.PP
The rnews program reads an article from standard input
and processes it for inclusion in the local news.
.I Rnews
removes all newsgroups
from the newsgroup line
to which the local system does not
subscribe (as specified in sys).
Articles that the local system does not subscribe to
at all
are thrown away.
As
.I rnews
copies an article into /usr/spool/news,
the local system name is prepended to the
system-pathname line.
After the article has been copied
the article is retransmitted,
as modified by the reception process,
to all systems which subscribe to the article.
Note that if system xyz
does not subscribe to net.test (for example),
it will not receive or retransmit
articles submitted only to net.test,
and will remove net.test
from the newsgroup line of
any article passing through xyz.
.NH
Examples and little documented features.
.NH 2
Bulk cancellation of news.
.PP
Most systems feel the need for
some automatic mechanism for disposing of news.
This shell script gets rid of all news
which is more than two weeks old:
.DS
while true
do
	echo 'c'
done 2>&1 | news -n all -b -2 weeks >/dev/null
.DE
A more convenient method for cancellation is in the works.
.NH 2
Daily shell script for news maintenance.
.PP
The file "news.daily" can be run via cron(8)
to perform bulk cancellation and other cleanups.
Look over that file to make sure it does what you want.
.NH 2
Users who should not get news.
.PP
.I News
assumes that all users should subscribe to
"general"
so that they will receive
administrative notices of system downtime &etc.
If an article is submitted to general,
then users will be greeted at login time with "You have news."
Some users (e.g. uucp) should not be bothered with such messages.
Fortunately it is possible, though inconvenient,
for a user to subscribe to nothing.
Suppose uid 253 should never receive news.
Manually change
the line in uindex that begins "253:"
to contain only a subscription to
"None"
.IP
253:328208485:None
.LP
Since "None" is a non-existent newsgroup,
uid 253 will never "have news."
.PP
Such users will still receive
the message of the day and "You have mail" notices,
so eliminating the news notice may be irrelevant.
Duke's login suppresses all messages
when the user has a non-standard shell (e.g. uucico).
.NH 2
Adding a new system to the news network
.PP
To tell news about a new system with which
it is to exchange news,
first add a line to the sys file:
.IP
newsys:net.all,to.newsys
.LP
If you cannot obtain
.I uux(1)
permission to run
.I rnews
on newsys,
then you will have use some other means
of transmitting articles,
and modify this sys entry accordingly.
Use of
.I mail(1)
to transmit articles is explained below.
Now add the test newsgroup to your ngfile:
.IP
echo "to.newsys" >>ngfile
.LP
and welcome the new system:
.DS
echo "welcome to USEnet" | news -i hello -n to.newsys
.DE
The new system should take corresponding action.
.NH 2
Subnetworks
.PP
The newsgroup matching scheme, together with
the system subscription lists (the sys file),
are designed to allow for the easy creation
of subnetworks.
To create the research triangle
subnetwork which consists of
only the newsgroup
"triangle,"
each of the triangle area systems
added
"triangle"
to the list of newsgroups
received, as specified
by their local entry in the sys file,
and added
"triangle"
to the list of newsgroups
sent to participating
systems.
.PP
For example, the sys file at U.N.C. might look like
.DS
unc:net.all,triangle,to.unc
duke:net.all,triangle,to.duke
.DE
.PP
If the triangle network needed
more than one newsgroup,
each system would put
"tri.all"
in their sys file
instead of
"triangle".
Since newsgroups ending
in
"all"
subscribe to any newsgroup
with the same prefix,
all newsgroups beginning
with
"tri."
would be transmitted throughout the
triangle subnetwork;
however, each newsgroup in the triangle network
should have a separate entry in the ngfile
since newsgroups containing
"all"
are not recommended for ngfile.
.NH 2
Setting up news to use mail
.PP
Suppose systems Able and Charlie wish to exchange
news, and that Able and Charlie talk to each
other only through Baker, a system which,
alas, does not run news.
Able can still exchange news with Charlie
using the transmission field in the sys
file
to transmit
news via
.I mail(1),
which understands indirection.
System Able would take the following steps
(and Charlie would take parallel action):
First, Able's sys line for Charlie should be
.DS L
Charlie:net.all,to.Charlie::sed -e "s/^/N/"|mail Baker!Charlie!news
.DE
The fourth field on the line is run to transmit news
to system Charlie by putting an 'N' on the front of
every line of the article and mailing it to Charlie!news.
Able sets up to receive news mailed from
Charlie by adding an entry in /etc/passwd
for the user "news"
and arranges to run the following
shell script once an hour or so.
.DS
(while true
do
	echo d
done) 2>&1 | mail -r -f /usr/spool/mail/news | uurec
.DE
This script pipes all the mail that
"news"
has received into
.I uurec,
a utility program distributed with
.I news.
.I Uurec
reads a series of articles,
splits them apart,
removes the "From" lines,
edits the contributor's path line
to reflect the systems through
which the article was mailed,
strips off the 'N'
on the front of all the article lines,
and passes the result to
.I rnews.
.I Uurec only understands the
.B A
format for network news.
However, since the
.B B
format generates what most mailers
consider to be a legal letter,
these kludges
may not be necessary.
If news is mailed across
a non uucp network,
then uurec.c may have to be modified
to correctly process the header information
supplied by the mail programs on other networks.
.NH
Security considerations.
.PP
It is easy to fake the origin of an article,
since
.I rnews
believes any
syntactically valid article
presented as standard input.
The only cure for this is a public key
encryption scheme.
.PP
Since
.I news
runs set-user-id,
it should not use a powerful
uid.
If
.I news
ran as "bin"
and a security hole were found in news,
the
entire system would be compromised.
.NH
Unimplemented features.
.NH 2
Read and write permissions on newsgroups.
.PP
Newsgroups should be protected from excessive
junk news by a permission scheme.
No elegant and general architecture
for newsgroup permissions has been proposed.
.NH 2
Per-user bit map.
.PP
.I News
should have a bit map (or equivalent) which indicates which
articles each user has yet to read.
This would allow
.I news
to permit the user to leave articles
around in an
undisposed
state.
.NH 2
Activation date.
.PP
News articles should support an activation date.
This would relieve users of the need to
submit an article at a specific time.
Use of
.I at(1)
and
.I calendar(1)
is recommended until such time as
.I news
supports that feature.
