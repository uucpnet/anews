#include "param.h"
#ifdef CTL_MSG
#define MAXARG	2	/* Max arguments used in ANY control message. */

/*
 * This file contains code to process all control messages.
 * Linkage to here is from input() via ctl() and control().
 *
 * If you're looking for security holes,
 * they're probably here.
 */

/*
 * Table of control commands.
 * Each returns TRUE if the message should be propagated.
 */

bool c_ihave(), c_sendme(), c_newgroup(), c_rmgroup();
bool c_cancel(), c_ssys(), c_suuname(), c_undef();

struct c_btab {
	char *c_cname;	/* Name of control command */
	int c_argc;	/* If nonzero, required number of arguments */
	int (*c_exec)(); /* action routine */
} c_btab[] = {
	"ihave",	3,	c_ihave,
	"sendme",	3,	c_sendme,
	"newgroup",	2,	c_newgroup,
	"rmgroup",	2,	c_rmgroup,
	"cancel",	2,	c_cancel,
	"sendsys",	1,	c_ssys,
	"senduuname",	1,	c_suuname,
	0,		0,	c_undef,
};


/*
 * Return true if header is for a control message.
 */
bool
ctl(hp)
register struct hbuf *hp;
{
	if (hp->h_control)
		return(TRUE);
	/* Backwards compatibility kludge follows.  Delete when not needed */
	if (ngmatch(hp->h_ngs, "all.all.ctl,")) {
		dstrcpy(&hp->h_control, hp->h_title);
		return(TRUE);
	}
	/* End of backwards compatibility kludge */
	return(FALSE);
}

/*
 * Process all control stuff.
 * This routine parses the control field and then switches
 * out to the individual routines.
 * Returns TRUE if the message should be propagated.
 */
bool
control(hp)
register struct hbuf *hp;
{
	register char *p;
	register struct c_btab *b;
	register int argc;
	char *cbfr, *(argv[MAXARG+2]);
	bool rc;

	sprintf(bfr, "CTL (%.20s,%.60s): %s",
		hp->h_name, hp->h_from, hp->h_control);
	log(bfr);

	/*
	 * Parse control line into arguments.
	 */
	cbfr = NULL;
	p = *dstrcpy(&cbfr, hp->h_control);
	hb_canon(p);	/* put the control field into nice stanard format */
	for (argc = 0; *(p = nonspace(p)) && argc < MAXARG+2; argc++) {
		argv[argc] = p;
		p = space(cbfr);
		if (*p)
			*p++ = '\0';
	}

	for (b = c_btab; b->c_cname; b++)
		if (strcmp(argv[0], b->c_cname) == 0)
			break;
	if (b->c_argc && b->c_argc != argc) {
		c_error(hp, "Arg count");
		rc = FALSE;
	}
	else
		rc = (*b->c_exec)(hp, argc, argv);
	dfree(&cbfr);
	return(rc);
}

/*
 * Somebody is telling us that they have some article,
 * but we don't care right now, as this stuff has not
 * yet been implemented.
 */
/*ARGSUSED*/
bool
c_ihave(hp, argc, argv)
struct hbuf *hp;
int argc;
char **argv;
{
	return(FALSE);
}

/*
 * Somebody wants an article.
 * Send it to them if they subscribe to it.
 */
/*ARGSUSED*/
bool
c_sendme(hp, argc, argv)
register struct hbuf *hp;
int argc;
register char **argv;
{
	if (!sysxmit(argv[1], argv[2])) {
		sprintf(bfr, "'%s' not in local SYSFILE file", argv[1]);
		c_error(hp, bfr);
	}
	return(FALSE);
}

/*
 * Create a new newsgroup.
 */
/*ARGSUSED*/
bool
c_newgroup(hp, argc, argv)
struct hbuf *hp;
int argc;
register char **argv;
{
	register char *s;
	register FILE *ngf;
	register bool found;

	found = FALSE;
	lock();
	ngf = xfopen(NGFILE, "r");
	while (!found && fgets(bfr, sizeof bfr, ngf) != NULL) {
		if (!nstrip(bfr))
			lxerror("NGFILE line too long");
		for (s=bfr; *s && *s != ' ' && *s != '\t' && *s != ':'; s++);
		*s = '\0';
		if (strcmp(argv[1], bfr) == 0)
			found = TRUE;
	}
	xfclose(ngf);
	unlock();
	if (!found)
		ngfappend(argv[1]);
	return(TRUE);
}

/*
 * Remove a newsgroup.
 */
/*ARGSUSED*/
bool
c_rmgroup(hp, argc, argv)
struct hbuf *hp;
int argc;
register char **argv;
{
	sprintf(bfr, "Request to remove newsgroup %s received", argv[1]);
	log(bfr);
	return(TRUE);
}

/*
 * Cancel an article
 */
/*ARGSUSED*/
bool
c_cancel(hp, argc, argv)
struct hbuf *hp;
int argc;
char **argv;
{
	struct nrec nrec;
	char name[NAMELEN+1];
	register bool found;

	fixartname(name, argv[1]);

	found = FALSE;
	lock();
	n_openm();
	while (n_read(&nrec))
		if (strcmp(nrec.n_name, name) != 0)
			n_write(&nrec);
		else
			found = TRUE;
	n_close();

	mkaname(lbfr, name);
	if (found)
		bitup = TRUE;
	else if (filelen(lbfr) == 0)
		log("... already cancelled");
	else
		log("... article not found");
	/* kill article, and make stub */
	canned(lbfr, name, !islocal(name) && isarticle(name));
	unlock();
	if (found)
		sleep(15);	/* give others a chance */
	return(TRUE);
}

/*
 * Send our .sys file
 *
 * This version sends the first NFIELDS fields
 * of each line in the .sys file.
 */
#define NFIELDS 2
/*ARGSUSED*/
bool
c_ssys(hp, argc, argv)
struct hbuf *hp;
int argc;
char **argv;
{
	register FILE *mf, *sf;
	register char *p;
	register int i;

	sprintf(bfr, "sys file from %s", SYSNAME);
	mf = mail_open(hp->h_from, hp->h_reply, bfr);
	sf = xfopen(SYSFILE, "r");
	while (fgets(bfr, MAXLEN, sf) != NULL) {
		p = bfr;
		for (i = NFIELDS; i--;)
			while (*p && *p != '\n' && *p++ != ':');
		if (p[-1] == ':')
			p--;
		*p++ = '\n';
		*p = '\0';
		fputs(bfr, mf);
	}
	xfclose(sf);
	mail_close(mf);
	sprintf(bfr, "Sending SYSFILE to %s", hp->h_from);
	log(bfr);
	return(TRUE);
}

/*
 * Send the output of running uuname
 */
static FILE *smf;
/*ARGSUSED*/
bool
c_suuname(hp, argc, argv)
register struct hbuf *hp;
int argc;
char **argv;
{
	int mailres();

#ifdef CTL_BUGOFF
	sprintf(bfr, "send uuname requests not honored by %s", SYSNAME);
	c_error(hp, bfr);
#else
	sprintf(bfr, "output of uuname on %s", SYSNAME);
	smf = mail_open(hp->h_from, hp->h_reply, bfr);
	fwsubr(mailres, "uuname", (char *)NULL);
	mail_close(smf);
#endif
	return(TRUE);
}

/*ARGSUSED*/
mailres(cmd, dummy)
register char *cmd;
char *dummy;
{
	close(1);
	dup2(1, fileno(smf));
	if (dup2(fileno(smf), 1) != 1)
		lxerror("mailres dup2 failed");
	nshell(cmd, (char *)NULL);
}

/*
 * Handle mangled or unknown control messages.
 */
/*ARGSUSED*/
bool
c_undef(hp, argc, argv)
register struct hbuf *hp;
int argc;
char **argv;
{
	sprintf(bfr, "Unknown ctl message: %s", hp->h_control);
	c_error(hp, bfr);
	return(FALSE);
}

/*
 * Mail an error message back to whomever.
 */
c_error(hp, s)
register struct hbuf *hp;
register char *s;
{
	register FILE *m;
	char buf[BUFLEN];

	if (hp->h_reply && *hp->h_reply)
		m = mail_open(hp->h_from, hp->h_reply, hp->h_name);
	else
		m = mail_open(hp->h_from, (char *)NULL, hp->h_name);

	sprintf(buf, "CTL error in %s: %s",hp->h_name, s);
	log(buf);
	fputs(buf, m);
	fputs("\nRejected command was:\n", m);
	h_bwrite(hp, m, FALSE);
	mail_close(m);
}
#endif
