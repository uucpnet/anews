#include "param.h"

static char TXFILE[BUFLEN];
#ifdef	SUBDIR
static char TMPXMIT[] = "etc/xmit";
#else
static char TMPXMIT[] = "xmit";
#endif

/*
 * Process inbound network news.
 */
uneti()
{
	register FILE *fp;
	register int c;
	register struct srec *srecp;
	register bool ok;
	off_t flen;

	/* copy article to a unique, named file. */
	if (TXFILE[0] == '\0')
		nmktemp(TXFILE, TMPXMIT);	/* generate unique filename */
	fp = xfopen(TXFILE, "w");
	while ((c = getc(stdin)) != EOF)
		putc(c, fp);
	xfclose(fp);

	/* process article */
	if ((fp = h_read(&header, TXFILE)) == NULL) {
		sprintf(bfr, "Inbound news (%s) is garbled",
			index(TXFILE, '.'));
		lxerror(bfr);
	}
	if (!isarticle(header.h_name)) {
		sprintf(bfr, "Bad article name '%s'", header.h_name);
		lxerror(bfr);
	}

	/* get the local system subscription line. */
	srecp = (struct srec *)xmalloc(sizeof(struct srec));
	if (s_find(srecp, SYSNAME) == NULL)
		lxerror("Local system not in sys file");
	ngsquash(header.h_ngs, srecp->s_sngs);
	free((char *)srecp);

	if (header.h_ngs[0] == '\0') {
		xfclose(fp);
		if ((fp = h_read(&header, TXFILE)) == NULL)
			lxerror("Cannot re-read inbound news");
		ngdel(header.h_ngs);
		sprintf(bfr, "Article %s (%s) not subscribed to",
			header.h_name, header.h_ngs);
		log(bfr);
		xfclose(fp);
		unlink(TXFILE);
	} else if ((flen = afilelen(header.h_name)) != -1) {
		sprintf(bfr, "Duplicate %s ignored", header.h_name);
		log(bfr);
		fprintf(stderr, "\tduplicate path: %s\n", header.h_from);
		xfclose(fp);
		if ((fp = ah_read(&header, header.h_name)) == NULL) {
			if (flen)
				lxerror("Original article is garbled");
			else
				fprintf(stderr, "\t(original is a stub)\n");
		}
		else {
			fprintf(stderr, "\t original path: %s\n", header.h_from);
			xfclose(fp);
		}
		unlink(TXFILE);
	} else {
		lock();
		ok = insert(fp);
		xfclose(fp);
		unlock();
		unlink(TXFILE);
		if (ok)
			broadcast(header.h_name, (char *)NULL);
#ifdef CTL_MSG
		else
			ctlrm(&header);
#endif
	}
	h_free(&header);
}

/*
 * Human input routine.
 */
iopt()
{
	register struct urec *urecp;
	time_t t;

	if (!o_iflag)
		xerror("No title given");
	if (!o_nflag)
		dngcat(dstrcpy(&header.h_ngs, INGDFLT));
	if (o_eflag) {
		/* Sanitize expiration date */
		if ((t = atot(header.h_edate)) < 0)
			xerror("Bad expiration date");
		nstrip(*dstrcpy(&header.h_edate, ctime(&t)));
	}
	nstrip(strcpy(header.h_adate, ctime(&now)));

	/* KLUDGE -- If followup, change net.general to net.followup */
	if (strcmp(header.h_ngs, "net.general,") == 0) {
		warn("This article is being posted to net.followup");
		dstrcpy(&header.h_ngs, "net.followup,");
	}

	/* Warn user if he doesnt subscribe to the newsgroup */
	urecp = (struct urec *)xmalloc(sizeof(struct urec));
	u_openr();
	while (u_read(urecp)) if (urecp->u_uid == uid) {
		if (!ngmatch(header.h_ngs, urecp->u_sngs)) {
			ngdel(header.h_ngs);
			sprintf(bfr, "you do not subscribe to '%s'", header.h_ngs);
			warn(bfr);
			ngcat(header.h_ngs);	/* dngcat() not needed here */
		}
		break;
	}
	u_close();
	free((char *)urecp);

	ngfcheck(FALSE);
	if (sigtrap)
		goodbye();
	lock();
	getname();
	unlock();
	getuser();
	if (insert(stdin))
		fsubr(broadcast, header.h_name, (char *)NULL);
#ifdef CTL_MSG
	else
		ctlrm(&header);
#endif
	h_free(&header);
}

/*
 * Process a news article.
 * The header is assumed to be set up in header,
 * and the body is assumed to be readable on stream ifp.
 * Returns TRUE if the article should be broadcast.
 */

static char NOTINS[] = "Not inserted\n";

insert(ifp)
FILE *ifp;
{
	register FILE *ofp, *tfp;
	register char *p;
	register int c, blines;
	register bool is_ctl, has_seen, lasttrap;
	struct nrec nrec;
	char tfname[BUFLEN], ofname[BUFLEN];

	if (strlen(header.h_ngs) > BUFLEN-20)
		lxerror("Newsgroup list too long");
	nstrip(header.h_title);	/* pull trailing space off title */
	while (header.h_title[0] == ' ')	/* pull leading space also */
		strcpy(&header.h_title[0], &header.h_title[1]);
	if (header.h_control)
		nstrip(header.h_control);	/* trim control message, too */

#ifdef CTL_MSG
	is_ctl = ctl(&header);
#else
	is_ctl = FALSE;
#endif

	/*
	 * Capture the article body.
	 */
	nmktemp(tfname, TMPXMIT);
	tfp = xfopen(tfname, "w");


	/* copy the body */
	blines = 0;		/* blank line count */
	has_seen = FALSE;
	lasttrap = FALSE;
	for (;;) {
		if (fgets((p = &bfr[0]), MAXLEN, ifp) == NULL) {
			clearerr(ifp);
			if (rnews || !fromtty)
				break;
			if (sigtrap) {
				if (lasttrap)
					break;
				lasttrap = TRUE;
				sigtrap = FALSE;
				printf("(Interrupt -- one more to kill article)\n");
				fflush(stdout);
				continue;
			}
			if (has_seen)
				break;
			else
				strcpy(p, ".\n");
		}
		lasttrap = FALSE;
		nstrip(p);
		if (p[0] == '.' && p[1] == '\0') {
			if (fromtty) {
				if (has_seen)
					break;
				else {
					printf("Type ~p to view article, then '.' to submit it.\n");
					printf("(continue)\n");
					fflush(stdout);
					continue;
				}
			}
			else {
				warn("'.' alone on line ignored");
				fflush(stdout);
			}
		}
		if (p[0]) {
			while (blines > 0) {
				fprintf(tfp, "\n");
				blines--;
			}
			if (!rnews && p[0] == '~') {
				xfclose(tfp);
				has_seen = doescape(p, tfname, &header);
				tfp = xfopen(tfname, "a");
				printf("(continue)\n");
				fflush(stdout);
				continue;
			}
			if (strncmp(p, "From ", 5) == 0)
				fprintf(tfp, ">");
			fprintf(tfp, "%s\n", p);
			has_seen = FALSE;
		}
		else
			blines++;
	}
	xfclose(tfp);

	if (sigtrap) {
		fprintf(stderr, NOTINS);
		if (filelen(tfname) != 0 && fromtty)
			fwsubr(newssave, tfname, (char *)NULL);
		unlink(tfname);
		return(FALSE);
	}
	if (fromtty)
		printf("EOT\n");
	fflush(stdout);

	lock();
	time(&now);		/* get updated current time */
	nstrip(strcpy(header.h_rdate, ctime(&now)));
	tfp = xfopen(tfname, "r");
	header.h_nlines = linecnt(tfp);
	mkaname(ofname, header.h_name);
	ofp = xfopen(ofname, "w");
	h_bwrite(&header, ofp, TRUE);
	while ((c = getc(tfp)) != EOF)
		putc(c, ofp);
	xfclose(ofp);
	xfclose(tfp);
	unlink(tfname);

#ifdef CTL_MSG
	/*
	 * Process control messages.
	 */
	if (is_ctl)
		return(control(&header));
#endif

	strcpy(nrec.n_name, header.h_name);
	strcpy(nrec.n_ings, header.h_ngs);
	nrec.n_date = now;
	newart(&nrec);	/* update nindex, uindex, bitfile */
	if (time((time_t *)0) == now)
		sleep(1);	/* ensure articles have unique date */
	unlock();
	return(TRUE);
}

/*
 * Handle escape sequences
 */
doescape(p, filename, hp)
register char *p, *filename;
register struct hbuf *hp;
{
	register FILE *afp, *tfp;
	register int c;
	char *epgm, fname[BUFLEN];
	int ufsave();

	switch(p[1]) {

	case 'p':	/* Print article */
		if (p[2])
			goto badescape;
		h_print(hp, FALSE, stdout);
		afp = xfopen(filename, "r");
		while ((c = getc(afp)) != EOF && !sigtrap)
			putc(c, stdout);
		xfclose(afp);
		return(TRUE);

	case 'e':	/* Edit with e */
		epgm = editor;
		goto doedit;

	case 'v':	/* Edit with vi */
		epgm = "vi";
		goto doedit;

	doedit:
		if (p[2])
			goto badescape;
		/* Create editor temporary */
		strcpy(fname, "/tmp/newsXXXXXX");
		mktemp(fname);
		if (fwsubr(ufsave, filename, fname)) {
			printf("Could not create editor temporary.\n");
			return(FALSE);
		}
		if ((tfp = fopen(fname, "r")) == NULL) {
			printf("Can not open editor temporary!\n");
			return(FALSE);
		}
		/* Do the edit */
		sprintf(bfr, "%s %s", epgm, fname);
		fwsubr(ushell, bfr, (char *)NULL);
		unlink(fname);
		/* Recopy article body */
		afp = xfopen(filename, "w");
		while ((c = getc(tfp)) != EOF)
			putc(c, afp);
		xfclose(tfp);
		xfclose(afp);
		return(FALSE);

	case '!':	/* Escape to shell */
		fwsubr(ushell, &p[2], (char *)NULL);
		printf("!\n");
		return(FALSE);

	case '?':	/* List available escapes */
		printf("\t~p	Print article\n");
		printf("\t~e	Use 'e' to edit the article body\n");
		printf("\t~v	Use 'vi' to edit the article body\n");
		printf("\t~!	Escape to shell\n");
		printf("\t~?	List available ~ escapes\n");
		return(FALSE);

	default:
	badescape:
		printf("Bad ~ escape, type ~? for complete list\n");
		return(FALSE);
	}
}

/*
 * Save partial news.
 */
/*ARGSUSED*/
newssave(name, dummy)
char *name, *dummy;
{
	register FILE *to, *from;
	char sfname[BUFLEN];
	register int c;

	if ((from = fopen(name, "r")) == NULL)
		return(0);
	revert();

	strcpy(sfname, PARTIAL);
	if ((to = fopen(sfname, "w")) == NULL) {
		sprintf(sfname, "%s/%s", userhome, PARTIAL);
		if ((to = fopen(sfname, "w")) == NULL)
			xerror("Cannot save partial news");
	}
	while ((c = getc(from)) != EOF)
		putc(c, to);
	xfclose(from);
	xfclose(to);
	printf("News saved in %s\n", sfname);
	return(0);
}

/*
 * Copy file from news area to user area.
 */
ufsave(name, dest)
char *name, *dest;
{
	register FILE *to, *from;
	register int c;

	from = xfopen(name, "r");
	revert();
	umask(umask(0)&~0644);	/* Make sure temporary is generally readable */
	to = xfopen(dest, "w");
	while ((c = getc(from)) != EOF)
		putc(c, to);
	xfclose(from);
	xfclose(to);
	return(0);
}

/*
 * Transmit this article to all interested systems.
 * If checkp is non-NULL, returns TRUE iff
 * article *would* be transmitted (looping permitted).
 */
broadcast(name, checkp)
char *name, *checkp;
{
	register char *nptr, *hptr;
	register struct srec *srecp;
	register FILE *fp;
	struct hbuf h;

	h_init(&h);
	if ((fp = ah_read(&h, name)) == NULL)
		lxerror("Cannot reread article");
	xfclose(fp);

	 /* combine from and path, then break into list of systems. */
	if (h.h_path) {
		dstrcat(&h.h_from, "!");
		dstrcat(&h.h_from, h.h_path);
	}
	hptr = nptr = h.h_from;
	while (*hptr != '\0') {
		if (index(NETCHRS, *hptr)) {
			*hptr++ = '\0';
			nptr = hptr;
		} else
			hptr++;
	}
	*nptr = '\0';

	/* loop once per system. */
	s_openr();
	srecp = (struct srec *)xmalloc(sizeof(struct srec));
	while (s_read(srecp)) {
		if (islocal(srecp->s_name))
			continue;
		if (!ngmatch(h.h_ngs, srecp->s_sngs))
			continue;
		if (checkp) {
			free((char *)srecp);
			h_free(&h);
			return(TRUE);
		}
		hptr = h.h_from;
		while (*hptr != '\0') {
			if (strncmp(srecp->s_name, hptr, SNLN) == 0)
				goto contin;
			while (*hptr++ != '\0');
		}
		/* Do same thing with path */
		transmit(srecp, name);
	contin:;
	}
	free((char *)srecp);
	s_close();
#ifdef CTL_MSG
	ctlrm(&h);
#endif
	h_free(&h);
	return(FALSE);
}

/*
 * Transmit file named 'fname' to system 'sys',
 * or everywhere is 'sys' is 'all'.
 * Returns TRUE if succeded, FALSE if 'sys' not in SYSFILE.
 */
sysxmit(sys, fname)
register char *sys, *fname;
{
	register struct srec *srecp;

	if (strcmp(sys, "all") == 0
	 || strcmp(sys, "All") == 0
	 || strcmp(sys, "ALL") == 0) {
		broadcast(fname, (char *)NULL);
		return(TRUE);
	}
	srecp = (struct srec *)xmalloc(sizeof(struct srec));
	if (s_find(srecp, sys)) {
		transmit(srecp, fname);
		free((char *)srecp);
		return(TRUE);
	}
	free((char *)srecp);
	return(FALSE);
}

/*
 * Transmit file to system.
 */
transmit(sp, name)
register struct srec *sp;
char *name;
{
	register FILE *ifp, *ofp;
	register int c;
	struct hbuf h;

	h_init(&h);
	if ((ifp = ah_read(&h, name)) == NULL)
		return;
	ngsquash(h.h_ngs, sp->s_sngs);
	if (h.h_ngs[0] == '\0') {
		sprintf(bfr, "Article not subscribed to by %s", sp->s_name);
		log(bfr);
		h_free(&h);
		return;
	}
	if (TXFILE[0] == '\0')
		nmktemp(TXFILE, TMPXMIT);
	ofp = xfopen(TXFILE, "w");
	if (sp->s_flags & S_AFLAG)
		h_awrite(&h, ofp);
	else if (sp->s_flags & S_BFLAG)
		h_bwrite(&h, ofp, FALSE);
	else	/* default transmission format */
		h_bwrite(&h, ofp, FALSE);
	while ((c = getc(ifp)) != EOF)
		putc(c, ofp);
	xfclose(ifp);
	xfclose(ofp);
	if (*sp->s_xmit == '\0')
		sprintf(bfr, "uux -r - %s!rnews", sp->s_name);
	else
		sprintf(bfr, "(%s)", sp->s_xmit);
	fwsubr(nshell, bfr, TXFILE);
	unlink(TXFILE);
	h_free(&h);
}

#ifdef CTL_MSG
/*
 * Remove control message.
 * Control articles are created but not merged into nindex.
 * Locally generated control messages are simply unlinked.
 * Remotely generated control messages are canned, leaving a stub.
 */
ctlrm(hp)
register struct hbuf *hp;
{
	if (!ctl(hp))
		return;
	mkaname(lbfr, hp->h_name);
	canned(lbfr, hp->h_name, !islocal(hp->h_name));
}
#endif
