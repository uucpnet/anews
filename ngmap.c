/* CAVEAT EMPTOR */
/*
 * This program prefixes specified news groups.
 * It reads the article on stnd in and puts it on stnd out.
 * Sample ".sys" line:
 *     mcnc:net,vlsi,dept,grad::ngmap duke. dept grad | uux -r - mcnc!rnews
 * (ngmap is in /bin or /usr/bin)
 * The system "mcnc" receives the "net" and "vlsi" newsgroups,
 * and also "dept" and "grad" but remapped as "duke.dept" and "duke.grad".
 */

#include <stdio.h>
#define	BUFLEN	2000
#define	NGDELIM	','
#define	MAIL	"/usr/bin/mail"

char	NGHDR[] = "Newsgroups: ";

/*
 * Edits the newsgroup line of an article.
 * The line has a list of news groups separated by commas.
 *
 * No check is made for the line exceeding BUFLEN characters.
 *
 * After the line is edited, it is printed on stdout.
 *
 * If only a prefix is specified, it is removed from all newsgroups.
 * If a prefix and groups are specifed, it is prepended to them.
 */

char **ngs;	/* pointer to the news groups */
int nngs;	/* how many of them. */
char *prefix;	/* prefix to add to news groups */

edit(s)
register char *s;
{
	register char *p, **q;
	register i;
	int first;

	/* first, convert all commas to '\0' */

	first = 1;

	for (p=s; *p; p++)
		if (*p == NGDELIM)
			*p = '\0';
	if (p[-1] == '\n')
		p[-1] = '\0';
	*++p = '\0';	/* terminate with double zero */

	/* now, loop over all newsgroups. */
	i = strlen(prefix);
	for (p=s; *p;) {
		if (!first)
			putchar(NGDELIM);
		first = 0;
		if (nngs) {
			/* if adding prefix, loop over all being checked */
			q = ngs;
			for (i=0; i<nngs; i++)
				if (strcmp(p,*q++) == 0)
					break;
			if (i < nngs)
				/* matched */
				fputs(prefix, stdout);
			fputs(p, stdout);
		} else {
			if (strncmp(prefix, p, i) == 0)
				fputs(p+i, stdout);
			else
				fputs(p, stdout);
		}
		while (*p++);
	}
	putchar('\n');
}

char *myname;
char *mail_to;

char buf[BUFLEN];

main(argc, argv)
int argc;
char **argv;
{
	register c;

	myname = *argv;

	if (argc < 2) {
		fprintf(stderr,
		    "usage: %s [-m mail_errors_to] prefix [ng1 ng2 ..]\n",
			myname);
		exit(1);
	}

	while (**++argv == '-') {
		switch (*++*argv) {

		case 'm':
			mail_to = *++argv;
			argc--;
			break;

		default:
			sprintf(buf, "bad option %c", **argv);
			error(buf);
			break;
		}
		argc--;
	}

	prefix = *argv;
	ngs = ++argv;
	nngs = argc -2;

	setbuf(stdout, SYSBUF);

	c = getchar();
	putchar(c);

	/* A format? */
	if (c == 'A') {
		fgets(buf, sizeof buf, stdin);
		fputs(buf, stdout);
		fgets(buf, sizeof buf, stdin);
		edit(buf);
	} else {
		/* Assume B format */
		for (;;) {
			if (fgets(buf, sizeof(buf), stdin) == NULL)
				error("Bad B format");
			if (strncmp(buf, NGHDR, sizeof(NGHDR)-1) == 0)
				break;
			fputs(buf, stdout);
		}
		printf(NGHDR);
		edit(&buf[sizeof(NGHDR)-1]);
	}
	while((c = getchar()) != EOF)
		putchar(c);
	exit(0);
}

/*
 * Report errors.
 */

error(s)
char *s;
{
	register c;
	FILE *errorf, *popen();

	if (mail_to) {
		sprintf(buf, "%s %s", MAIL, mail_to);
		if ((errorf = popen(buf, "w")) != NULL) {
			fprintf(errorf,"%s: %s\n", myname, s);
			fclose(errorf);
		}
	} else
		fprintf(stderr, "%s: %s\n", myname, s);

	while ((c = getchar()) != EOF)
		putchar(c);
	exit(1);
}
