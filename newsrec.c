/*	MCNC	newsrec.c	1.2	8/30/82	18:04:26	*/
/* Yuck, recover "nindex" files that got clobbered by
 * a news malfunction.
 *
 * Usage:
 *	newsrecover [-submit]
 *
 *	-submit argument causes date to be taken from the submission
 *	date of article, file modification time otherwise.
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/stat.h>

#ifndef NEWSDIR
# define NEWSDIR	"."
#endif

#ifdef lint
# define IGNORE(X)	__void__=(int)(X)
  int __void__;
#else
# define IGNORE(X)	X
#endif
#define EOS	'\0'
#define NO	0
#define YES	1
#define	NHW	2	/* Number of Headers Wanted for .nindex info */
#define	MHL	256	/* Max Header Length */

static char	Sccsid[] = "@(#)newsrec.c	1.2";
char	newsdir[]	= NEWSDIR;
char	linebuf[256];
char	*headers[NHW+1] = {
		"Newsgroups: ", "Posted: ", NULL};
#define	NEWSGROUPS	0
#define	POSTED		1
char	headstr[NHW][MHL], c;
int	headflag;
		
char *index(), *rindex();
int submit = NO;

main(argc, argv)
char **argv;
{
	long timeret, timenow;
	register FILE *filef, *dirf;
	struct direct dirbuf;
	struct stat sb;

	if (argc > 1 && strcmp (argv[1], "-submit") == 0)
		submit = YES;
	IGNORE (time (&timenow));
	if (chdir (newsdir))
		fperror ("cannot change directory to %s", newsdir);
	if ((dirf = fopen (".", "r")) < 0)
		fperror ("cannot open %s", newsdir);
	while (fread ((char *)&dirbuf, sizeof dirbuf, 1, dirf) == 1) {
		char *p;
		char temp[DIRSIZ+1];
		char junk[DIRSIZ];
		int ijunk, sjunk;

		if (dirbuf.d_ino == 0)
			continue;
		strncpy (temp, dirbuf.d_name, sizeof dirbuf.d_name);
		/* skip non system name files */
		if ((sjunk = sscanf (temp, "%[^.].%d", junk, &ijunk)) != 2)
			continue;
		if ((filef = fopen (temp, "r")) == NULL) {
			fpremark ("Cannot open %s", temp);
			continue;
		}
		fstat (fileno(filef), &sb);
			/* See if Anews or Bnews */
		if ((c=getc(filef)) == 'A') {
			if (  (fgets (linebuf, MHL, filef) == NULL)
			    ||(fgets (headstr[NEWSGROUPS], MHL, filef) == NULL)
			    ||(fgets (linebuf, MHL, filef) == NULL)
			    ||(fgets (headstr[POSTED], MHL, filef) == NULL)) {
				IGNORE (fclose (filef));
				continue;
			}
			rmnl(headstr[NEWSGROUPS]);
			rmnl(headstr[POSTED]);
			headflag = 0;
		} else {
		 ungetc(c, filef);
			/* clear headers */
		 for (headflag=NHW-1; headflag >= 0; --headflag)
			headstr[headflag][0] = NULL;
			/* get headers */
		 for (headflag=NHW; headflag > 0; ) {
			register int i;
			if (fgets (linebuf, sizeof linebuf, filef) == NULL) {
				headflag = -1;
				break;
			}
			/*
			 * look for headers and copy them to headstr
			 */
			for (i = 0; i < NHW; ++i) {
				if (strncmp(headers[i], linebuf, strlen(headers[i]))==0) {
					/* count headers seen */
					if (headstr[i][0] == NULL)
						--headflag;
					{register int temp;
					 temp=strlen(headers[i]);
					 strncpy(headstr[i], linebuf+temp, MHL-temp);
					}
					rmnl (headstr[i]);
				}
			}
			/* done with current header - go on */
		 }
		}
		IGNORE (fclose (filef));
		if (headflag == -1)
			continue;
		/* terrible hack:  the format of date strings simply will
		 * not pass through the getdate routine.  Chop off the first
		 * field (the day) and the last field (the year),
		 * and everything should be OK.
		 */
		if (submit == YES) {
			p = index(headstr[POSTED],' ');
			p++;
			strcpy (headstr[POSTED], p);
			p = rindex (headstr[POSTED], ' ');
			*p = 0;
			timeret = getdate (headstr[POSTED], &timenow);
		}
		else
			timeret = sb.st_mtime;
		printf ("%s:%D:%s\n", temp, timeret, headstr[NEWSGROUPS]);
	}
}

extern int errno;
extern char *sys_errlist[];
fperror (gripe, args)
char *gripe;
{
	int sverrno = errno;

	_doprnt (gripe, &args, stderr);
	fprintf (stderr, ": %s\n", sys_errlist[sverrno]);
	exit (-1);
}

fpremark (gripe, args)
char *gripe;
{
	int sverrno = errno;

	_doprnt (gripe, &args, stderr);
	fprintf (stderr, ": %s\n", sys_errlist[sverrno]);
}

rmnl (str)
char *str;
{
	char *p;
	char *index();

	if (p = index (str, '\n'))
		*p = EOS;
}
